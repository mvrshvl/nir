package database

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	"sync/atomic"

	"github.com/jmoiron/sqlx"

	"aml/amlerror"
	logging "aml/log"
)

type AccountType int64

const (
	EOA AccountType = iota
	MinerAccount
	PaymentManager
	Wallet
	ExchangeLR
	ExchangeP2PLR
	Market
	ExchangeP2PHR
	ExchangeHR
	ExchangeRM
	ATM
	ExchangeVHR
	Mixer
	Gambling
	Scam
	Stolen
	ExchangeFraudulent
	Ransom
	IllegalService
	DarkMarket
	DarkService
	Deposit
	Token

	Cluster     AccountType = -1
	errAccounts             = amlerror.AMLError("can't get transfer accounts")
)

type Accounts []*Account

type Account struct {
	Address string      `db:"address"`
	AccType AccountType `db:"accountType"`
	Cluster *uint64     `db:"cluster"`
	Comment *string     `db:"comment"`
}

type Transactor interface {
	sqlx.ExecerContext
	QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error)
}

func (accounts Accounts) AddAccounts(ctx context.Context, db Transactor, update bool) error {
	fmtValues := "(%q, %v, %q),"

	values := ""

	cacheAccs := make(map[string]struct{})

	for _, account := range accounts {
		isExistInDB, err := account.isAccountExist(ctx, db)
		if err != nil {
			return err
		}

		_, isExistLocal := cacheAccs[account.Address]

		if isExistInDB || isExistLocal {
			err := UpdateAccountType(ctx, db, account.Address, account.AccType)
			if err != nil {
				return err
			}

			continue
		}

		cacheAccs[account.Address] = struct{}{}

		if account.Comment == nil {
			values += fmt.Sprintf("(%q, %v, %v),", strings.ToLower(account.Address), account.AccType, "NULL")
			continue
		}

		values += fmt.Sprintf(fmtValues, strings.ToLower(account.Address), account.AccType, account.Comment)
	}

	if len(values) == 0 {
		return nil
	}
	_, err := db.ExecContext(ctx,
		fmt.Sprintf(`INSERT INTO accounts (address, accountType, comment)
				VALUES %s`, values[:len(values)-1]),
	)
	if err != nil {
		return fmt.Errorf("can't add accounts: %w", err)
	}

	return nil
}

func (account Account) isAccountExist(ctx context.Context, db Transactor) (bool, error) {
	rows, err := db.QueryContext(ctx, `SELECT * FROM accounts
				WHERE address = ?`, strings.ToLower(account.Address))
	if err != nil {
		return false, err
	}

	accounts, err := scanAccounts(rows)
	if err != nil {
		return false, err
	}

	return len(accounts) != 0, nil
}

func UpdateAccountType(ctx context.Context, db Transactor, address string, accType AccountType) error {
	_, err := db.ExecContext(ctx,
		`UPDATE accounts SET accountType = ? WHERE address = ?`,
		accType, strings.ToLower(address))
	if err != nil {
		return fmt.Errorf("can't update account type: %w", err)
	}

	return nil
}

func (db *Connection) GetAccount(ctx context.Context, address string) (*Account, error) {
	query := `SELECT * FROM accounts
				WHERE address = ?`

	acc := new(Account)

	err := db.connection.GetContext(ctx, acc, query, strings.ToLower(address))
	if err != nil {
		return nil, fmt.Errorf("can't get account %s: %w", address, err)
	}

	return acc, nil
}

func (db *Connection) GetAccounts(ctx context.Context, addresses ...string) ([]*Account, error) {
	query := `SELECT * FROM accounts
				WHERE address IN ( ? )`

	queryIn, args, err := sqlx.In(query, addresses)
	if err != nil {
		return nil, fmt.Errorf("can't create IN QUERY: %w", err)
	}

	rows, err := db.connection.QueryContext(ctx, queryIn, args...)
	if err != nil {
		return nil, fmt.Errorf("can't get accounts: %w", err)
	}

	defer rows.Close()

	return scanAccounts(rows)
}

func scanAccounts(rows *sql.Rows) ([]*Account, error) {
	var accounts []*Account

	for rows.Next() {
		var acc Account

		err := rows.Scan(
			&acc.Address,
			&acc.AccType,
			&acc.Comment,
			&acc.Cluster,
		)
		if err != nil {
			return nil, err
		}

		acc.Address = strings.ToLower(acc.Address)

		accounts = append(accounts, &acc)
	}

	return accounts, nil
}

func (db *Connection) GetDepositSenders(ctx context.Context, address string, excludeAddresses ...string) ([]string, error) {
	query := `SELECT DISTINCT FromAddress FROM transactions
				LEFT JOIN accounts
				ON transactions.FromAddress = accounts.address
				WHERE toAddress = ?
				  AND NOT accountType IN ( ? )`

	queryIn, args, err := sqlx.In(query, strings.ToLower(address), GetExchanges())
	if err != nil {
		return nil, fmt.Errorf("can't create IN QUERY: %w", err)
	}

	return db.getAddresses(ctx, queryIn, excludeAddresses, args...)
}

func (db *Connection) GetDepositsByAddresses(ctx context.Context, addresses []string, excludeAddresses ...string) ([]string, error) {
	query := `SELECT DISTINCT transactions.toAddress FROM transactions
				LEFT JOIN accounts
				ON transactions.toAddress = accounts.address
				WHERE accounts.accountType = ?
				AND transactions.FromAddress IN ( ? )`

	queryIn, args, err := sqlx.In(query, Deposit, addresses)
	if err != nil {
		return nil, fmt.Errorf("can't create IN QUERY: %w", err)
	}

	return db.getAddresses(ctx, queryIn, excludeAddresses, args...)
}

func (db *Connection) getAddresses(ctx context.Context, query string, excludeAddresses []string, args ...interface{}) ([]string, error) {
	rows, err := db.connection.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, fmt.Errorf("can't get query database: %w", err)
	}

	defer rows.Close()

	if rows.Err() != nil {
		return nil, fmt.Errorf("can't get addresses: %w", rows.Err())
	}

	var senders []string

	for rows.Next() {
		var sender string

		err := rows.Scan(
			&sender,
		)
		if err != nil {
			return nil, err
		}

		if isExcluded(sender, excludeAddresses) {
			continue
		}

		senders = append(senders, sender)
	}

	return senders, nil
}

func isExcluded(sender string, excludeAddresses []string) bool {
	for _, excludeAddr := range excludeAddresses {
		if sender == excludeAddr {
			return true
		}
	}

	return false
}

func (db *Connection) UpdateClusterByAddress(ctx context.Context, cluster uint64, addresses ...string) error {
	query := `UPDATE accounts SET cluster = ? WHERE address IN ( ? )`

	queryIn, args, err := sqlx.In(query, cluster, addresses)
	if err != nil {
		return fmt.Errorf("can't create IN QUERY for cluster updating: %w", err)
	}

	_, err = db.connection.ExecContext(ctx, queryIn, args...)
	if err != nil {
		return fmt.Errorf("can't update cluster by address: %w", err)
	}

	return nil
}

func (db *Connection) UpdateClusterByCluster(ctx context.Context, src, dst uint64) error {
	query := `UPDATE accounts SET cluster = ? WHERE cluster = ?`

	logging.Debugf(ctx, "merging cluster %v into %v", src, dst)

	_, err := db.connection.ExecContext(ctx, query, dst, src)
	if err != nil {
		return fmt.Errorf("can't update cluster by cluster: %w", err)
	}

	return nil
}

func (db *Connection) CreateCluster(ctx context.Context, heuristic string) (uint64, error) {
	query := `INSERT INTO clusters(heuristic) VALUES (?)`

	res, err := db.connection.ExecContext(ctx, query, heuristic)
	if err != nil {
		return 0, fmt.Errorf("can't create cluster: %w", err)
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}

	return uint64(id), nil
}

func (db *Connection) DeleteCluster(ctx context.Context, id uint64) error {
	query := `DELETE FROM clusters WHERE id = ?`

	logging.Debugf(ctx, "deleting cluster %v", id)

	_, err := db.connection.ExecContext(ctx, query, id)
	if err != nil {
		return fmt.Errorf("can't delete cluster: %w", err)
	}

	return nil
}

func (db *Connection) MergeClusters(ctx context.Context, src, dst uint64) error {
	err := db.UpdateClusterByCluster(ctx, src, dst)
	if err != nil {
		return err
	}

	return db.DeleteCluster(ctx, src)
}

func (db *Connection) GetSenderAndReceiver(ctx context.Context, fromAddress, toAddress string) (sender, receiver *Account, err error) {
	accounts, err := db.GetAccounts(ctx, fromAddress, toAddress)
	if err != nil {
		return nil, nil, err
	}

	if fromAddress == toAddress && len(accounts) == 1 {
		return accounts[0], accounts[0], nil
	}

	for _, acc := range accounts {
		switch strings.ToLower(acc.Address) {
		case fromAddress:
			sender = acc
		case toAddress:
			receiver = acc
		}
	}

	if sender == nil {
		return nil, nil, fmt.Errorf("%w: sender address %s", errAccounts, fromAddress)
	}

	if receiver == nil {
		return nil, nil, fmt.Errorf("%w: receiver address %s", errAccounts, toAddress)
	}

	return sender, receiver, nil
}

func (db *Connection) UpdateCluster(ctx context.Context, sender, receiver *Account, heuristic string, clusteringBySender, clusteringByReceiver UpdateCluster, createCluster CreateCluster) error {
	switch true {
	case sender.Cluster != nil && receiver.Cluster != nil && atomic.LoadUint64(sender.Cluster) != atomic.LoadUint64(receiver.Cluster):
		return db.MergeClusters(ctx, *receiver.Cluster, *sender.Cluster)
	case sender.Cluster != nil && receiver.Cluster != nil && atomic.LoadUint64(sender.Cluster) == atomic.LoadUint64(receiver.Cluster):
		return nil
	case sender.Cluster != nil && receiver.Cluster == nil:
		return clusteringBySender(ctx, db, sender, receiver)
	case sender.Cluster == nil && receiver.Cluster != nil:
		return clusteringByReceiver(ctx, db, sender, receiver)
	case sender.Cluster == nil && receiver.Cluster == nil:
		return createCluster(ctx, db, sender, receiver, heuristic)
	default:
		logging.Debugf(ctx, "Unexpected way to find relations: sender %+v, Deposit %+v", sender, receiver)

		return nil
	}
}

func (db *Connection) GetAccountTypesRisks(ctx context.Context) (map[AccountType]int, error) {
	query := `SELECT idx, risk FROM accountType`

	rows, err := db.connection.QueryContext(ctx, query)
	if err != nil {
		return nil, fmt.Errorf("get addresses query error: %w", err)
	}

	if rows.Err() != nil {
		return nil, fmt.Errorf("can't get addresses: %w", rows.Err())
	}

	risks := make(map[AccountType]int)

	for rows.Next() {
		var (
			idx  AccountType
			risk int
		)

		err := rows.Scan(&idx, &risk)
		if err != nil {
			return nil, err
		}

		risks[idx] = risk
	}

	return risks, nil
}

func (acc AccountType) GetDescription() string {
	switch acc {
	case Cluster:
		return "Кластер"
	case EOA:
		return "Обычный аккаунт"
	case MinerAccount:
		return "Майнер"
	case PaymentManager:
		return "Платёжный сервис"
	case Wallet:
		return "Кошелек"
	case ExchangeLR:
		return "Биржа"
	case ExchangeP2PLR:
		return "P2P Биржа"
	case Market:
		return "Магазин"
	case ExchangeP2PHR:
		return "P2P Биржа с малым лимитом"
	case ExchangeHR:
		return "Биржа с малым лимитом"
	case ExchangeRM:
		return "Биржа с большим лимитом"
	case ATM:
		return "Банкомат"
	case ExchangeVHR:
		return "Биржа без идентификации пользователя"
	case Mixer:
		return "Миксер"
	case Gambling:
		return "Азартные игры"
	case Scam:
		return "Мошенник"
	case Stolen:
		return "Украденный аккаунт"
	case ExchangeFraudulent:
		return "Биржа мошенник"
	case Ransom:
		return "Выкуп"
	case IllegalService:
		return "Нелегальные услуги"
	case DarkMarket:
		return "Магазин на черном рынке"
	case DarkService:
		return "Теневой сервис"
	case Deposit:
		return "Депозит"
	case Token:
		return "Токен"
	default:
		return ""
	}
}
