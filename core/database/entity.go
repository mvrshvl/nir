package database

import (
	"context"
	"fmt"
)

type Entity struct {
	Name     string
	Accounts []*Account
}

func (db *Connection) GetEntity(ctx context.Context, address string) (*Entity, error) {
	acc, err := db.GetAccount(ctx, address)
	if err != nil {
		return nil, err
	}

	entity := &Entity{}

	if *acc.Cluster == 0 {
		entity.Name = "Single account"

		return entity, nil
	}

	entity.Accounts, err = db.getEntityAccounts(ctx, *acc.Cluster)
	if err != nil {
		return nil, fmt.Errorf("get enity accounts failed: %w", err)
	}

	entity.Name = fmt.Sprintf("cluster-%v", *acc.Cluster)

	return entity, err
}

func (db *Connection) getEntityAccounts(ctx context.Context, cluster uint64) ([]*Account, error) {
	query := `SELECT * FROM accounts
				WHERE cluster = ?
				AND NOT accountType = ?`

	rows, err := db.connection.QueryContext(ctx, query, cluster, Deposit)
	if err != nil {
		return nil, fmt.Errorf("can't get cluster accounts: %w", err)
	}

	defer rows.Close()

	return scanAccounts(rows)
}

func (db *Connection) GetEntitiesAccounts(ctx context.Context) ([]*Account, error) {
	query := `SELECT * FROM accounts
				WHERE cluster IS NOT NULL`

	rows, err := db.connection.QueryContext(ctx, query)
	if err != nil {
		return nil, fmt.Errorf("can't get cluster accounts: %w", err)
	}

	defer rows.Close()

	return scanAccounts(rows)
}
