package database

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"math/big"
	"sort"
	"strings"

	"aml/amlerror"
	logging "aml/log"
	"github.com/ethereum/go-ethereum/common/hexutil"
)

type TxType string

const (
	address0          = "0x0000000000000000000000000000000000000000"
	TxApprove  TxType = "approve"
	TxTransfer TxType = "transfer"
	TxDeploy   TxType = "deploy"

	errTxToDepositNotFound = amlerror.AMLError("transaction to deposit not found")
)

type Transaction struct {
	Hash             string   `csv:"hash" db:"hash"`
	Nonce            uint64   `csv:"nonce" db:"nonce"`
	BlockNumber      uint64   `csv:"block_number" db:"blockNumber"`
	TransactionIndex *uint64  `csv:"transaction_index" db:"transactionIndex"`
	FromAddress      string   `csv:"from_address" db:"fromAddress"`
	ToAddress        string   `csv:"to_address" db:"toAddress"`
	Value            *big.Int `csv:"value" db:"value"`
	Gas              *big.Int `csv:"gas" db:"gas"`
	GasPrice         *big.Int `csv:"gas_price" db:"gasPrice"`
	Input            string   `csv:"input" db:"input"`
	ContractAddress  *string  `db:"contractAddress"`
	Type             *TxType  `db:"type"`
}

type Transactions []*Transaction

func (tx *Transaction) AddTransaction(ctx context.Context, dbTx *sql.Tx) error {
	_, err := dbTx.ExecContext(ctx,
		`INSERT INTO transactions(hash, nonce, blockNumber, transactionIndex, FromAddress, toAddress, value, gas, gasPrice, input)
    			VALUES(?,?,?,?,?,?,?,?,?,?)`,
		tx.Hash, tx.Nonce, tx.BlockNumber, tx.TransactionIndex, strings.ToLower(tx.FromAddress), strings.ToLower(tx.ToAddress),
		toHexadecimal(tx.Value), toHexadecimal(tx.Gas), toHexadecimal(tx.GasPrice), tx.Input)

	if err != nil {
		return fmt.Errorf("can't add tx: %w", err)
	}

	return Accounts{}.AddAccounts(ctx, dbTx, false)
}

func toHexadecimal(value *big.Int) []byte {
	return []byte(hexutil.EncodeBig(value))
}

func fromHexadecimal(value string) (*big.Int, error) {
	return hexutil.DecodeBig(value)
}

func (txs Transactions) AddTransactions(ctx context.Context, dbTx *sql.Tx) error {
	fmtValues := "(%q,%v,%v,%v,%q,%q,%q,%q,%q,%q),"

	values := ""

	var accounts Accounts

	for i, tx := range txs {
		if len(tx.Input) > 1000 {
			tx.Input = tx.Input[:1000]
		}
		values += fmt.Sprintf(fmtValues, tx.Hash, tx.Nonce, tx.BlockNumber, *tx.TransactionIndex, strings.ToLower(tx.FromAddress), strings.ToLower(tx.ToAddress),
			toHexadecimal(tx.Value), toHexadecimal(tx.Gas), toHexadecimal(tx.GasPrice), tx.Input)

		accounts = append(accounts, &Account{
			Address: tx.FromAddress,
			AccType: EOA,
		}, &Account{
			Address: tx.ToAddress,
			AccType: EOA,
		})

		if i > 0 && i%1000 == 0 { // промежуточная вставка, строка слишком забивается
			err := insertTxs(ctx, dbTx, values, accounts)
			if err != nil {
				return err
			}

			accounts = nil
			values = ""
		}
	}

	if len(values) == 0 {
		return nil
	}

	return insertTxs(ctx, dbTx, values, accounts)
}

func insertTxs(ctx context.Context, dbTx *sql.Tx, values string, accounts Accounts) error {
	_, err := dbTx.ExecContext(ctx, fmt.Sprintf(`INSERT INTO transactions(hash, nonce, blockNumber, transactionIndex, FromAddress, toAddress, value, gas, gasPrice, input)
    			VALUES %s`, values[:len(values)-1]))
	if err != nil {
		return fmt.Errorf("can't add txs: %w", err)
	}

	return accounts.AddAccounts(ctx, dbTx, false)
}

func UpdateTxType(ctx context.Context, dbTx *sql.Tx, hash, toAddress, contractAddr string, value *big.Int, t TxType) error {
	_, err := dbTx.ExecContext(ctx,
		`UPDATE transactions SET toAddress = ?, contractAddress = ?, type = ?, value = ? WHERE hash = ?`,
		strings.ToLower(toAddress), strings.ToLower(contractAddr), t, toHexadecimal(value), hash)

	if err != nil {
		return fmt.Errorf("can't update tx: %w", err)
	}

	return nil
}

func (txs TokenTransfers) UpdateTokenTransfers(ctx context.Context, dbTx *sql.Tx) error {
	for _, tx := range txs {
		if tx.SourceAddress == address0 {
			err := UpdateTxType(ctx, dbTx, tx.TxHash, "", tx.ContractAddress, tx.Value, TxDeploy)
			if err != nil {
				return err
			}

			continue
		}

		err := UpdateTxType(ctx, dbTx, tx.TxHash, tx.TargetAddress, tx.ContractAddress, tx.Value, TxTransfer)
		if err != nil {
			return err
		}
	}

	return nil
}

func (txs ERC20Approves) UpdateApproves(ctx context.Context, dbTx *sql.Tx) error {
	for _, tx := range txs {
		err := UpdateTxType(ctx, dbTx, tx.TxHash, tx.ToAddress, tx.ContractAddress, big.NewInt(0), TxApprove)
		if err != nil {
			return err
		}
	}

	return nil
}

func (db *Connection) GetTxsToExchange(ctx context.Context, fromBlock, toBlock uint64) (Transactions, error) {
	query := `SELECT hash, nonce, blockNumber, fromAddress, toAddress, value, gas, gasPrice, input, contractAddress, type FROM transactions
    				LEFT JOIN exchanges AS exchanges
    				ON transactions.toAddress = exchanges.Address
					LEFT JOIN exchanges AS deposit
					ON transactions.fromAddress = deposit.Address
					WHERE transactions.blockNumber BETWEEN ? AND ?
					  AND exchanges.address IS NOT NULL
					  AND deposit.address IS NULL`

	rows, err := db.connection.QueryContext(ctx, query, fromBlock, toBlock)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}

		return nil, fmt.Errorf("can't get exchange transfers: %w", err)
	}

	txsToExchange, err := scanTransactions(rows)
	if err != nil {
		return nil, err
	}

	return txsToExchange, nil
}

func (db *Connection) GetExchangeTransfers(ctx context.Context, txsToExchange Transactions, diffBlock uint64, diffGasKoef uint64) ([]*ExchangeTransfer, error) {
	transfers := make([]*ExchangeTransfer, 0, len(txsToExchange))

	for _, txToExchange := range txsToExchange {
		txToDeposit, err := db.getExchangeTransfer(ctx, txToExchange, diffBlock, diffGasKoef)
		if err != nil {
			logging.Debugf(ctx, "get exchange transfer %s failed: %v", txToExchange.Hash, err)
			continue
		}

		_, err = db.connection.ExecContext(ctx,
			`INSERT INTO exchangeTransfers(txDeposit, txExchange) VALUES(?,?)`,
			txToDeposit.Hash, txToExchange.Hash)
		if err != nil {
			return nil, fmt.Errorf("can't add ExchangeAccount transfer: %w", err)
		}

		err = UpdateAccountType(ctx, db.connection, txToExchange.FromAddress, Deposit)
		if err != nil {
			return nil, fmt.Errorf("can't update account %q type 'deposit': %w", txToExchange.FromAddress, err)
		}

		transfers = append(transfers, &ExchangeTransfer{
			TxExchange: txToExchange.Hash,
			TxDeposit:  txToDeposit.Hash,
		})
	}

	return transfers, nil
}

func (db *Connection) getExchangeTransfer(ctx context.Context, txToExchange *Transaction, diffBlock uint64, diffGasPercent uint64) (*Transaction, error) {
	query := `SELECT hash, nonce, blockNumber, fromAddress, toAddress, value, gas, gasPrice, input, contractAddress, type FROM transactions
    				LEFT JOIN exchangeTransfers
    				ON transactions.hash = exchangeTransfers.txDeposit
					LEFT JOIN exchanges
					ON transactions.FromAddress = exchanges.address
					WHERE transactions.toAddress = ?
					  AND transactions.blockNumber BETWEEN ? AND ?
					  AND exchangeTransfers.txDeposit IS NULL 
					  AND exchanges.address IS NULL` // filter txs from exchange to deposit

	minBlock, maxBlock := getMinMaxBlocks(txToExchange, diffBlock)

	rows, err := db.connection.QueryContext(ctx, query, txToExchange.FromAddress,
		minBlock, maxBlock)
	if err != nil {
		return nil, fmt.Errorf("can't get tx to Deposit: %w", err)
	}

	txs, err := scanTransactions(rows)
	if err != nil {
		return nil, err
	}

	matchedTx := filterByValue(txs, txToExchange, int64(diffGasPercent))
	if matchedTx == nil {
		logging.Debugf(ctx, "tx to Deposit not found (tx to exchange %s, range blocks %v-%v, value %v, to Address %v)", txToExchange.Hash, minBlock, maxBlock, txToExchange.Value, txToExchange.ToAddress)

		return nil, errTxToDepositNotFound
	}

	return matchedTx, nil
}

func (db *Connection) GetTransactionsByAddress(ctx context.Context, address string) (Transactions, error) {
	rows, err := db.connection.QueryContext(ctx, `SELECT hash, nonce, blockNumber, fromAddress, toAddress,
				value, gas, gasPrice, input, contractAddress, type FROM transactions
					WHERE  toAddress = ?
					AND value IS NOT NULL`, address)
	if err != nil {
		return nil, fmt.Errorf("can't get txs by address: %w", err)
	}

	txs, err := scanTransactions(rows)
	if err != nil {
		return nil, err
	}

	rows, err = db.connection.QueryContext(ctx, `SELECT hash, nonce, blockNumber, fromAddress, toAddress,
				value, gas, gasPrice, input, contractAddress, type FROM transactions
					WHERE  fromAddress = ?
					AND value IS NOT NULL`, address)
	if err != nil {
		return nil, fmt.Errorf("can't get txs by address: %w", err)
	}

	txsFrom, err := scanTransactions(rows)
	if err != nil {
		return nil, err
	}

	return append(txsFrom, txs...), nil
}

func getMinMaxBlocks(tx *Transaction, diffBlock uint64) (minBlock, maxBlock uint64) {
	if tx.BlockNumber > diffBlock {
		minBlock = tx.BlockNumber - diffBlock
	}

	if tx.BlockNumber > 1 {
		maxBlock = tx.BlockNumber - 1
	}

	return
}

const fullPercents = 100

func filterByValue(txs Transactions, tx *Transaction, diffGasPercent int64) *Transaction {
	minValue := big.NewInt(0)

	if tx.Value.Int64() > tx.Gas.Int64()*tx.GasPrice.Int64()*diffGasPercent {
		gas := big.NewInt(0).Mul(tx.Gas, tx.GasPrice)

		gasWithDiff := big.NewInt(0).Mul(gas, big.NewInt(diffGasPercent))
		gasWithDiff = gasWithDiff.Div(gasWithDiff, big.NewInt(fullPercents))

		minValue = minValue.Sub(tx.Value, gasWithDiff)
	}

	var matchesTxs Transactions

	for _, matchedTx := range txs {
		compareMin := matchedTx.Value.Cmp(minValue)
		compareMax := matchedTx.Value.Cmp(tx.Value)

		if compareMax == 0 {
			return matchedTx
		}

		if compareMin == 1 && compareMax == -1 {
			matchesTxs = append(matchesTxs, matchedTx)
		}
	}

	if len(matchesTxs) == 0 {
		return nil
	}

	sort.Slice(matchesTxs, func(i, j int) bool {
		return matchesTxs[i].Value.Cmp(matchesTxs[j].Value) == -1
	})

	return matchesTxs[len(matchesTxs)-1]
}

func scanTransactions(rows *sql.Rows) (txs Transactions, err error) {
	for rows.Next() {
		var (
			tx                   Transaction
			value, gas, gasPrice string
		)

		err = rows.Scan(
			&tx.Hash,
			&tx.Nonce,
			&tx.BlockNumber,
			&tx.FromAddress,
			&tx.ToAddress,
			&value,
			&gas,
			&gasPrice,
			&tx.Input,
			&tx.ContractAddress,
			&tx.Type,
		)

		if err != nil {
			return nil, fmt.Errorf("scan txs error: %w", err)
		}

		tx.Value, err = fromHexadecimal(value)
		if err != nil {
			return nil, fmt.Errorf("scan value error: %w", err)
		}

		tx.Gas, err = fromHexadecimal(gas)
		if err != nil {
			return nil, fmt.Errorf("scan gas error: %w", err)
		}

		tx.GasPrice, err = fromHexadecimal(gasPrice)
		if err != nil {
			return nil, fmt.Errorf("scan gasPrice error: %w", err)
		}

		if err != nil {
			return nil, fmt.Errorf("scan txs error: %w", err)
		}

		txs = append(txs, &tx)
	}

	return txs, nil
}

func (txs Transactions) GetFromAddresses() (addresses []string) {
	for _, tx := range txs {
		addresses = append(addresses, tx.FromAddress)
	}

	return
}

func (txs Transactions) GetTransactionsToAddresses(addresses map[string]struct{}) Transactions {
	var txsToAddress Transactions

	for _, tx := range txs {
		if _, ok := addresses[tx.ToAddress]; ok {
			txsToAddress = append(txsToAddress, tx)
		}
	}

	return txsToAddress
}

func (txs Transactions) MapFromAddresses() map[string]struct{} {
	addresses := make(map[string]struct{})
	for _, exch := range txs {
		addresses[exch.FromAddress] = struct{}{}
	}

	return addresses
}
