package database

import (
	"context"
	"database/sql"
	"embed"
	"fmt"
	"io/fs"
	"net/http"

	//_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	migrate "github.com/rubenv/sql-migrate"

	"aml/config"
	"aml/di"
)

//go:embed migrations/*.sql
var migrationsPath embed.FS

//go:generate mockgen -destination=mock_database_generated.go -package=database . Database
type Database interface {
	GetAccount(ctx context.Context, address string) (*Account, error)
	GetAccounts(ctx context.Context, addresses ...string) ([]*Account, error)
	GetDepositSenders(ctx context.Context, address string, excludeAddresses ...string) ([]string, error)
	GetDepositsByAddresses(ctx context.Context, addresses []string, excludeAddresses ...string) ([]string, error)
	UpdateClusterByAddress(ctx context.Context, cluster uint64, addresses ...string) error
	UpdateClusterByCluster(ctx context.Context, src uint64, dst uint64) error
	CreateCluster(ctx context.Context, heuristic string) (uint64, error)
	DeleteCluster(ctx context.Context, id uint64) error
	MergeClusters(ctx context.Context, src uint64, dst uint64) error
	GetSenderAndReceiver(ctx context.Context, fromAddress string, toAddress string) (sender *Account, receiver *Account, err error)
	UpdateCluster(ctx context.Context, sender *Account, receiver *Account, heuristic string, clusteringBySender, clusteringByReceiver UpdateCluster, createCluster CreateCluster) error
	GetAccountTypesRisks(ctx context.Context) (map[AccountType]int, error)
	Connect(ctx context.Context) error
	GetConnection() *sqlx.DB
	GetAirdrops(ctx context.Context, fromBlock uint64, toBlock uint64, minTransfers uint64) ([]*Airdrop, error)
	FindTransfersBetweenMembers(ctx context.Context, a *Airdrop) (Transactions, error)
	FilterOwners(ctx context.Context, airdrops []*Airdrop) (filtered []*Airdrop, err error)
	GetSelfApproveTxs(ctx context.Context, fromBlock uint64, toBlock uint64, maxApproves uint64) (Transactions, error)
	GetLastBlock(ctx context.Context) (uint64, error)
	ExecuteTx(ctx context.Context, fn func(ctx context.Context, tx *sql.Tx) error) error
	GetEntity(ctx context.Context, address string) (*Entity, error)
	GetEntitiesAccounts(ctx context.Context) ([]*Account, error)
	GetTxsToExchange(ctx context.Context, fromBlock, toBlock uint64) (Transactions, error)
	GetExchangeTransfers(ctx context.Context, txsToExchange Transactions, diffBlock uint64, diffGasPercent uint64) ([]*ExchangeTransfer, error)
	getExchangeTransfer(ctx context.Context, txToExchange *Transaction, diffBlock uint64, diffGasPercent uint64) (*Transaction, error)
	GetTransactionsByAddress(ctx context.Context, address string) (Transactions, error)
	AddExchangeTransfer(ctx context.Context, txExchangeHash string, txDepositHash string) error
	GetTransferTxs(ctx context.Context, transfer *ExchangeTransfer) (txToDeposit *Transaction, txToExchange *Transaction, err error)
}

type CreateCluster func(ctx context.Context, db Database, from *Account, to *Account, heuristic string) error
type UpdateCluster func(ctx context.Context, db Database, from *Account, to *Account) error

type Connection struct {
	connection *sqlx.DB
}

func New() Database {
	return &Connection{}
}

func (db *Connection) Connect(ctx context.Context) error {
	return di.FromContext(ctx).Invoke(func(cfg *config.Config) error {
		err := db.connect(ctx, cfg, "%s:%s@tcp(%s)/%s?parseTime=true")
		if err != nil {
			return err
		}

		if cfg.Database.Clean {
			err := db.migrate(cfg, migrate.Down)
			if err != nil {
				return err
			}
		}

		return db.migrate(cfg, migrate.Up)
	})
}

func (db *Connection) GetConnection() *sqlx.DB {
	return db.connection
}

func (db *Connection) migrate(cfg *config.Config, direction migrate.MigrationDirection) error {
	migrationsDirectory, err := fs.Sub(migrationsPath, "migrations")
	if err != nil {
		return err
	}

	migrations := &migrate.HttpFileSystemMigrationSource{FileSystem: http.FS(migrationsDirectory)}

	_, err = migrate.Exec(db.connection.DB, cfg.Database.Driver, migrations, direction)
	if err != nil {
		return err
	}

	_, err = db.connection.Exec("SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));")

	return err
}

func (db *Connection) connect(ctx context.Context, cfg *config.Config, dsn string) (err error) {
	db.connection, err = sqlx.ConnectContext(ctx, cfg.Database.Driver, fmt.Sprintf(dsn, cfg.Database.User, cfg.Database.Password, cfg.Database.Address, cfg.Database.Name))
	if err != nil {
		return fmt.Errorf("failed to connect db: %w", err)
	}

	return nil
}
