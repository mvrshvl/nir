package database

import (
	"context"
	"fmt"
	"strings"
)

type Exchange struct {
	Address string      `csv:"address" db:"address"`
	Name    string      `csv:"name" db:"name"`
	AccType AccountType `csv:"type" db:"accType"`
}

type Exchanges []*Exchange

func (exchanges Exchanges) MapAddresses() map[string]struct{} {
	exchs := make(map[string]struct{})
	for _, exch := range exchanges {
		exchs[exch.Address] = struct{}{}
	}

	return exchs
}

func (ex *Exchange) AddExchange(ctx context.Context, db Transactor) error {
	_, err := db.ExecContext(ctx,
		`INSERT INTO exchanges(address, name)
    			VALUES(?,?)`,
		strings.ToLower(ex.Address), ex.Name)

	if err != nil {
		return fmt.Errorf("can't add exchange: %w", err)
	}

	return Accounts{&Account{
		Address: ex.Address,
		AccType: ex.AccType,
		Cluster: nil,
		Comment: &ex.Name,
	}}.AddAccounts(ctx, db, true)
}

func (exchanges Exchanges) AddExchanges(ctx context.Context, db Transactor) error {
	for _, exchange := range exchanges {
		err := exchange.AddExchange(ctx, db)
		if err != nil {
			if strings.Contains(err.Error(), "Duplicate entry") {
				err = UpdateAccountType(ctx, db, exchange.Address, exchange.AccType)
				if err != nil {
					return err
				}
				continue
			}
			return err
		}
	}

	return nil
}

func GetExchanges() []AccountType {
	return []AccountType{
		ExchangeLR, ExchangeHR, ExchangeVHR, ExchangeP2PLR, ExchangeP2PHR, ExchangeRM, ExchangeFraudulent,
	}
}
