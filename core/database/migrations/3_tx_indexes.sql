-- +migrate Up

CREATE INDEX tx_from ON transactions(fromAddress);
CREATE INDEX tx_to ON transactions(toAddress);
CREATE INDEX deposit ON transactions(toAddress, blockNumber);
CREATE INDEX txTypes ON transactions(type, blockNumber);
CREATE INDEX approveFrom ON transactions(fromAddress, contractAddress);
CREATE INDEX approveTo ON transactions(toAddress, contractAddress);
CREATE INDEX tx_to_value ON transactions(toAddress, value);
CREATE INDEX tx_from_value ON transactions(fromAddress, value);
-- +migrate Down

DROP INDEX tx_from_value ON transactions;
DROP INDEX tx_from ON transactions;
DROP INDEX  tx_to ON transactions;
DROP INDEX deposit ON transactions;
DROP INDEX txTypes ON transactions;
DROP INDEX approveFrom ON transactions;
DROP INDEX approveTo ON transactions;