-- +migrate Up

CREATE INDEX account_type ON accounts(address, accountType);

-- +migrate Down

DROP INDEX account_type ON accounts;