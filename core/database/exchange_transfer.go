package database

import (
	"context"
	"fmt"

	"aml/amlerror"
)

const errTransfer = amlerror.AMLError("can't find tx from transfer")

type ExchangeTransfer struct {
	TxExchange string `db:"txExchange"`
	TxDeposit  string `db:"txDeposit"`
}

func (db *Connection) AddExchangeTransfer(ctx context.Context, txExchangeHash, txDepositHash string) error {
	_, err := db.connection.ExecContext(ctx,
		`INSERT INTO exchangeTransfers(txDeposit, txExchange)
    			VALUES(?,?)`,
		txDepositHash, txExchangeHash)
	if err != nil {
		return fmt.Errorf("can't add exchange transfer: %w", err)
	}

	return nil
}

func (db *Connection) GetTransferTxs(ctx context.Context, transfer *ExchangeTransfer) (txToDeposit, txToExchange *Transaction, err error) {
	query := `SELECT hash, nonce, blockNumber, fromAddress, toAddress, value, gas, gasPrice, input, contractAddress, type FROM transactions
				WHERE hash = ?
 				   OR hash = ?`

	rows, err := db.connection.QueryContext(ctx, query, transfer.TxDeposit, transfer.TxExchange)
	if err != nil {
		return nil, nil, err
	}

	txs, err := scanTransactions(rows)
	if err != nil {
		return nil, nil, err
	}

	txToDeposit, err = getTxByHash(transfer.TxDeposit, txs)
	if err != nil {
		return nil, nil, fmt.Errorf("%w: to deposit %s", err, transfer.TxDeposit)
	}

	txToExchange, err = getTxByHash(transfer.TxExchange, txs)
	if err != nil {
		return nil, nil, fmt.Errorf("%w: to exchange %s", err, transfer.TxExchange)
	}

	return
}

func getTxByHash(hash string, txs Transactions) (*Transaction, error) {
	for _, tx := range txs {
		if tx.Hash == hash {
			return tx, nil
		}
	}

	return nil, errTransfer
}
