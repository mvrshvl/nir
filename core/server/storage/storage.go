package storage

import (
	"context"
	"sync"

	"aml/amlerror"
	"aml/core/linking"
	"aml/di"
)

const (
	errReportNotFound = amlerror.AMLError("report not found")
)

type Storage struct {
	mux     sync.RWMutex
	storage map[string]*Report
}

type Report struct {
	Links  *linking.Links
	Report []byte
}

func New() *Storage {
	return &Storage{
		storage: make(map[string]*Report),
	}
}

func (s *Storage) Add(report *Report) {
	s.mux.Lock()
	defer s.mux.Unlock()

	s.storage[report.Links.GetID()] = report
}

func (s *Storage) Get(key string) (*Report, error) {
	s.mux.RLock()
	defer s.mux.RUnlock()

	report, ok := s.storage[key]
	if !ok {
		return nil, errReportNotFound
	}

	return report, nil
}

func GetWithCtx(ctx context.Context, key string) (report *Report, err error) {
	err = di.FromContext(ctx).Invoke(func(s *Storage) (innerErr error) {
		report, innerErr = s.Get(key)
		return
	})

	return report, err
}

func AddWithCtx(ctx context.Context, report *Report) error {
	return di.FromContext(ctx).Invoke(func(s *Storage) {
		s.Add(report)
	})
}
