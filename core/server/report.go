package server

import (
	gecko "aml/core/linking/geckocoin"
	"context"
	"fmt"
	"math/big"
	"strings"
	"time"

	"aml/core/database"
	"aml/core/linking"
	"aml/core/linking/node"
	"aml/core/linking/node/rate"
	"aml/core/linking/prices"
	"aml/di"
)

const (
	fmtReportPage = `
					<!DOCTYPE HTML>
					<html>
					<head>
						<meta charset="utf-8">
						<title>Отчет</title>
					</head>
					<body bgcolor="242424">
					<table border="0" width="80%%" cellspacing="0" cellpadding="10" style="margin-left: 10%%; margin-top: 20px" bgcolor="#f0f8ff">
						<tbody>
						%s
						</tbody>
					</table>
					</body>
					</html>
`

	fmtAccount = `	<td style="width: 50%%; text-align: left;">
            			<p style="text-align: left; margin-left: 10px; font-size: 20px;"><font face="monospace">Адрес: %s</font></p>
            			<p style="text-align: left; margin-left: 10px; font-size: 20px;"><font face="monospace">Тип аккаунта: %s</font></p>
						<p style="text-align: start; margin-left: 10px; font-size: 20px;"><font face="monospace">Время: %s</font></p>
						<p style="text-align: start; margin-left: 10px; font-size: 20px;"><font face="monospace">Номер блока: %v</font></p>
					</td>`

	fmtRate = `	<td style="width: 50%%; text-align: left;">
            		<p style="font-size: 60px;"><strong><font color="%s">%.2f</font></strong></p>
        		</td>`

	fmtCluster = `	<td style="width: 50%%; text-align: left;">
						<p style="text-align: start; margin-left: 10px; font-size: 20px;"><font face="monospace"><strong>Аккаунт принадлежит кластеру</strong></font></p>
						<p style="text-align: start; margin-left: 10px; font-size: 20px;"><font face="monospace">Размер: %v аккаунт(ов)</font></p>
            			<p style="text-align: start; margin-left: 10px; font-size: 20px;"><font face="monospace">Риск получения транзакции: %v%%</font></p>
            			<p style="text-align: start; margin-left: 10px; font-size: 20px;"><font face="monospace">Риск отправки транзакции: %v%%</font></p>
        			</td>`

	fmtSendRisks = `	<td style="width: 50%%; text-align: left;">
            			<p style="text-align: start; margin-left: 10px; font-size: 20px;"><font face="monospace">Риск отправки транзакции: %v%%</font></p>
						<ul>%s</ul>
        				</td>`

	fmtReceiveRisks = `	<td style="width: 50%%; text-align: left;">
            			<p style="text-align: start; margin-left: 10px; font-size: 20px;"><font face="monospace">Риск получения транзакции: %v%%</font></p>
						<ul>%s</ul>
        				</td>`

	fmtLinks = `	<td>
            		<p style="text-align: left; margin-left: 10px; font-size: 150%%;"><font face="monospace">Графы</font></p>
            		<ul>%s</ul>
        			</td>`

	fmtLink = `<li style="text-align: left; margin-left: 10px; font-size: 150%%;"><font face="monospace"><a href="%s" target="_blank">%s</a></font></li>`

	fmtBalance = `	<td>
            			<p style="text-align: left; margin-left: 10px; font-size: 150%%;"><font face="monospace">Баланс</font></p>
            			<ul>%s</ul>
        			</td>`

	fmtReceivedCurrencies = `	<td style="width: 50%%; text-align: left;">
            						<p style="text-align: left; margin-left: 10px; font-size: 150%%;"><font face="monospace">Полученные средства</font></p>
            						<ul>%s</ul>
        						</td>`

	fmtSentCurrencies = `	<td style="width: 5%0%; text-align: left;">
            					<p style="text-align: left; margin-left: 10px; font-size: 150%%;"><font face="monospace">Отправленные средства</font></p>
            					<ul>%s</ul>
        					</td>`

	fmtDocs = `	<td style="width: 5%0%; text-align: left;">
            					<p style="text-align: left; margin-left: 10px; font-size: 150%%;"><font face="monospace">Сопутствующие документы</font></p>
            					<ul>
									<li style="text-align: left; margin-left: 10px; font-size: 150%%;"><font face="monospace"><a href="/report/%s/raw" target="_blank">Анализированные данные</a></font></li>
									<li style="text-align: left; margin-left: 10px; font-size: 150%%;"><font face="monospace"><a href="/docs" target="_blank">Алгоритм оценки</a></font></li>
								</ul>
        					</td>`

	fmtList = `<li style="text-align: left; margin-left: 10px; font-size: 150%%;"><font face="monospace">%s</font></li>`

	templateInfo = `<tr>%s</tr>`

	redColor    = "9b111e"
	orangeColor = "ff9500"
	greenColor  = "oa6522"
)

func newReport(node *linking.Links, blockNumber uint64, timestamp time.Time) string {
	mainNode := node.GetMainNode()

	commonInfo := fmt.Sprintf(templateInfo, accountInfo(mainNode, blockNumber, timestamp)+rating(mainNode))
	ratingInfo := fmt.Sprintf(templateInfo, risks(fmtSendRisks, int(mainNode.GetRisk().GetSendRisk()), mainNode.GetRisk().Send.ByTypes)+risks(fmtReceiveRisks, int(mainNode.GetRisk().GetReceiveRisk()), mainNode.GetRisk().Receive.ByTypes))

	if cluster, ok := node.GetRoot().GetCluster(); ok {
		ratingInfo += fmt.Sprintf(templateInfo, clusterInfo(cluster)+rating(cluster))
	}

	docs := fmt.Sprintf(templateInfo, fmt.Sprintf(fmtDocs, node.ID))

	if len(mainNode.GetLinks()) == 0 {
		return fmt.Sprintf(fmtReportPage, commonInfo+ratingInfo+docs)
	}

	balance, sent, received := mainNode.GetBalanceByCurrency()
	curPrices := node.GetPrices()

	additionalInfo := fmt.Sprintf(templateInfo, graphLinks(node)+currencies(balance, fmtBalance, curPrices))
	additionalInfo += fmt.Sprintf(templateInfo, currencies(received, fmtReceivedCurrencies, curPrices)+currencies(sent, fmtSentCurrencies, curPrices))

	return fmt.Sprintf(fmtReportPage, commonInfo+ratingInfo+additionalInfo+docs)
}

func accountInfo(node *node.SingleNode, blockNumber uint64, timestamp time.Time) string {
	return fmt.Sprintf(fmtAccount, node.GetName(), node.GetAccountType().GetDescription(), timestamp.Format(time.UnixDate), blockNumber)
}

func rating(node node.Node) string {
	r := node.GetRisk().GetGeneralRate()
	color := redColor

	if r > rate.MediumRate {
		color = greenColor
	} else if r > rate.LowRate {
		color = orangeColor
	}

	return fmt.Sprintf(fmtRate, color, r)
}

func risks(format string, risk int, list map[string]float64) string {
	var risksByTypes string
	for t, risk := range list {
		var li string

		if risk < 1 {
			li = fmt.Sprintf(fmtList, fmt.Sprintf("%s: <1%%", t))
		} else {
			li = fmt.Sprintf(fmtList, fmt.Sprintf("%s: %.2f%%", t, risk))
		}

		risksByTypes += li
	}

	return fmt.Sprintf(format, risk, risksByTypes)
}

func clusterInfo(cluster *node.Cluster) string {
	return fmt.Sprintf(fmtCluster, len(cluster.GetLinks()), int(cluster.GetRisk().GetReceiveRisk()), int(cluster.GetRisk().GetSendRisk()))
}

func graphLinks(node *linking.Links) string {
	currencies := node.GetCurrencies()

	linksList := fmt.Sprintf(fmtLink, fmt.Sprintf("/report/%s/graph", node.GetID()), "All currencies")

	if len(currencies) > 1 {
		for currency := range currencies {
			linksList += fmt.Sprintf(fmtLink, fmt.Sprintf("/report/%s/graph?currency=%s", node.GetID(), currency), currency)
		}
	}

	return fmt.Sprintf(fmtLinks, linksList)
}

func currencies(balance map[string]*big.Int, format string, prices *prices.Prices) string {
	list := getCurrenciesList(balance, prices)
	if len(list) == 0 {
		return ""
	}

	return fmt.Sprintf(format, list)
}

func getCurrenciesList(currencies map[string]*big.Int, curPrices *prices.Prices) string {
	var currenciesList []string

	for currency, value := range currencies {
		if value.Uint64() == 0 || curPrices.Get(currency) <= 0 {
			continue
		}

		if currency != gecko.EthCurrency {
			currencyInfo := fmt.Sprintf(`%s: %s`, currency, value.String())
			currenciesList = append(currenciesList, fmt.Sprintf(fmtList, currencyInfo))
		} else {
			valueFloat := big.NewFloat(0).SetInt(value)
			valueFloat = big.NewFloat(0).Quo(valueFloat, big.NewFloat(gecko.WeiInEth))
			currencyInfo := fmt.Sprintf(`%s: %s`, currency, valueFloat.String())
			currenciesList = append(currenciesList, fmt.Sprintf(fmtList, currencyInfo))
		}
	}

	return strings.Join(currenciesList, "")
}

func getCurrentBlock(ctx context.Context) (block uint64, err error) {
	err = di.FromContext(ctx).Invoke(func(db database.Database) (innerErr error) {
		block, innerErr = db.GetLastBlock(ctx)

		return innerErr
	})

	return
}
