package rate

import (
	"fmt"
	"math"
	"sort"
	"strings"
)

const (
	MaxRate    = 10
	MediumRate = 8
	LowRate    = 4

	maxPathForEachRate = 3
)

type Risk struct {
	edgesHashes []string
	Send        DirectedRisk
	Receive     DirectedRisk
}

type DirectedRisk struct {
	Risk    float64
	Money   float64
	ByTypes map[string]float64
}

func New(branch map[string]int) *Risk {
	return &Risk{edgesHashes: []string{calculateHash(branch)}, Send: DirectedRisk{ByTypes: make(map[string]float64)}, Receive: DirectedRisk{ByTypes: make(map[string]float64)}}
}

// риск спонсирования злоумышленников на отправленные деньги
func (r *Risk) CalculateSendRisk(receiverSendRisk float64, money float64, risksByTypes map[string]float64, sumMoneySent float64) {
	r.Send.Risk += calculateRisk(receiverSendRisk, money, sumMoneySent)

	for accType, risk := range risksByTypes {
		r.Send.ByTypes[accType] += calculateRisk(risk, money, sumMoneySent)
	}
}

// риск получения грязных денег
func (r *Risk) CalculateReceiveRisk(senderReceiveRisk float64, money float64, risksByTypes map[string]float64, sumMoneyReceived float64) {
	r.Receive.Risk += calculateRisk(senderReceiveRisk, money, sumMoneyReceived)

	for accType, risk := range risksByTypes {
		r.Receive.ByTypes[accType] += calculateRisk(risk, money, sumMoneyReceived)
	}
}

func calculateRisk(risk float64, moneyWithRisk float64, allMoney float64) float64 {
	if allMoney == 0 {
		return 0
	}

	return risk * moneyWithRisk / allMoney
}

func (r Risk) GetGeneralRate() float64 {
	max := math.Max(r.Send.Risk, r.Receive.Risk)
	min := math.Min(r.Send.Risk, r.Receive.Risk)

	var generalRisk float64

	if max == min {
		generalRisk = (r.Send.Risk + r.Receive.Risk) / 2
	} else {
		generalRisk = max/1.5 + min/2
	}

	return getRate(generalRisk)
}

func (r Risk) GetReceiveRisk() float64 {
	return r.Receive.Risk
}

func (r Risk) GetSendRisk() float64 {
	return r.Send.Risk
}

func (r Risk) GetReceiveRate() float64 {
	return getRate(r.Receive.Risk)
}

func (r Risk) GetSendRate() float64 {
	return getRate(r.Send.Risk)
}

func getRate(risk float64) float64 {
	return MaxRate - risk/MaxRate
}

func (r *Risk) CalculateFromRate(newRate *Risk) {
	r.Send.Risk, r.Send.Money, r.Send.ByTypes = calculateFromRate(r.Send, newRate.Send)
	r.Receive.Risk, r.Receive.Money, r.Receive.ByTypes = calculateFromRate(r.Receive, newRate.Receive)

	r.edgesHashes = append(r.edgesHashes, newRate.edgesHashes...)
}

func calculateFromRate(oldRisk, newRisk DirectedRisk) (float64, float64, map[string]float64) {
	sumMoney := oldRisk.Money + newRisk.Money

	if sumMoney == 0 {
		return 0, 0, nil
	}

	if oldRisk.ByTypes != nil {
		for accType, percentageContent := range oldRisk.ByTypes {
			newPercentageContent, ok := newRisk.ByTypes[accType]
			if !ok {
				continue
			}

			oldRisk.ByTypes[accType] = ((percentageContent * oldRisk.Money) + (newPercentageContent * newRisk.Money)) / sumMoney
		}

		for accType, percentageContent := range newRisk.ByTypes {
			if _, ok := oldRisk.ByTypes[accType]; !ok {
				oldRisk.ByTypes[accType] = percentageContent
			}

			oldRisk.ByTypes[accType] = ((percentageContent * oldRisk.Money) + (newRisk.ByTypes[accType] * newRisk.Money)) / sumMoney
		}
	}

	return ((oldRisk.Risk * oldRisk.Money) + (newRisk.Risk * newRisk.Money)) / sumMoney, sumMoney, oldRisk.ByTypes
}

func calculateHash(branch map[string]int) string {
	if len(branch) == 0 {
		return "0"
	}

	var edges []string
	for edge, order := range branch {
		edges = append(edges, fmt.Sprintf("%v-%s", order, edge))
	}

	sort.Strings(edges)

	return strings.Join(edges, ",")
}

func (r *Risk) IsCalculatedEdges(branch map[string]int) bool {
	newHash := calculateHash(branch)

	for _, hash := range r.edgesHashes {
		if hash == newHash {
			return true
		}
	}

	return len(r.edgesHashes) > maxPathForEachRate
}
