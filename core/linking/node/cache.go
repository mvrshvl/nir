package node

type NodesStorage struct {
	Storage map[string]Node
}

func NewCache() *NodesStorage {
	return &NodesStorage{map[string]Node{}}
}
func (c *NodesStorage) Add(key string, n Node) {
	c.Storage[key] = n
}

func (c *NodesStorage) Get(key string) Node {
	return c.Storage[key]
}

func (c *NodesStorage) GetAll() map[string]Node {
	return c.Storage
}
