package node

import (
	"context"

	"aml/core/database"
	"aml/core/linking/node/rate"
	"aml/core/linking/prices"
	"aml/di"
)

type Node interface {
	GetName() string
	Link(ctx context.Context, depth int) error
	AddTx(address string, tx *database.Transaction)
	GetTxsByAddress(address string) map[string]database.Transactions
	GetTxs() database.Transactions
	IsAccountTypeToStop() bool
	GetRisk() *rate.Risk
	CalculateRisk(branch map[string]int, curPrices *prices.Prices, risksByTypes map[database.AccountType]int)
	CalculateTurnoverByAddress(address string, curPrices *prices.Prices) (in float64, out float64)
	GetNode(address string) *SingleNode
	GetLinks() map[string]struct{}
	CopyByCurrency(storage *NodesStorage, currency string) Node
	GetCluster() (*Cluster, bool)
}

func New(ctx context.Context, address string, cache *NodesStorage) (Node, error) {
	acc, err := GetAccount(ctx, address)
	if err != nil {
		return nil, err
	}

	if acc.Cluster == nil || acc.AccType == database.Deposit {
		singleNode := NewSingleNode(acc, cache)
		cache.Add(acc.Address, singleNode)

		return singleNode, nil
	}

	clusterRoot, err := NewCluster(ctx, address, cache)
	if err != nil {
		return nil, err
	}

	cache.Add(clusterRoot.GetName(), clusterRoot)

	return clusterRoot, nil
}

func GetAccount(ctx context.Context, address string) (acc *database.Account, err error) {
	err = di.FromContext(ctx).Invoke(func(db database.Database) (innerErr error) {
		acc, innerErr = db.GetAccount(ctx, address)

		return innerErr
	})

	return
}
