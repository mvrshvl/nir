package node

import (
	"math/big"
	"testing"

	"aml/core/database"
	gecko "aml/core/linking/geckocoin"
	"aml/core/linking/prices"
)

func TestRisk(t *testing.T) {
	// A [100, 0] -> (B,C) [100, 0] -> D [100, 100]
	//			  -> E [0, 0]
	expectedGeneralRate := 10 - 45/(1.5*10)
	expectedSendRate := 10 - 45.0/10
	expectedReceiveRate := 10.0 - 0

	testCache := NewCache()

	nodeA := NewSingleNode(&database.Account{
		Address: "A",
		AccType: database.EOA,
	}, testCache)

	nodeB := NewSingleNode(&database.Account{
		Address: "B",
		AccType: database.EOA,
	}, testCache)

	nodeC := NewSingleNode(&database.Account{
		Address: "C",
		AccType: database.EOA,
	}, testCache)

	nodeD := NewSingleNode(&database.Account{
		Address: "D",
		AccType: database.Scam,
	}, testCache)

	nodeE := NewSingleNode(&database.Account{
		Address: "E",
		AccType: database.MinerAccount,
	}, testCache)

	clusterBCD := new(Cluster)

	testCache.Add("A", nodeA)
	testCache.Add("B", clusterBCD)
	testCache.Add("C", clusterBCD)
	testCache.Add("D", nodeD)
	testCache.Add("E", nodeE)

	txAToB := NewTx("txAToB", "A", "B", 100000000)
	txBToD := NewTx("txBToD", "B", "D", 50000000)
	txCToE := NewTx("txCToE", "C", "E", 50000000)

	nodeA.addLink("B", txAToB)
	nodeB.addLink("A", txAToB)

	nodeB.addLink("D", txBToD)
	nodeD.addLink("B", txBToD)

	nodeC.addLink("E", txCToE)
	nodeE.addLink("C", txCToE)

	clusterBCD.Children = map[string]*SingleNode{
		"B": nodeB,
		"C": nodeC,
	}

	clusterBCD.name = "BC"

	p := &prices.Prices{
		Currency: map[string]float64{
			gecko.EthCurrency: 3000,
		},
	}

	riskByTypes := map[database.AccountType]int{
		database.EOA:          0,
		database.Scam:         90,
		database.MinerAccount: 0,
	}

	nodeA.CalculateRisk(nil, p, riskByTypes)

	if nodeA.Risk.GetGeneralRate() != expectedGeneralRate {
		t.Fatalf("unexpected general rate: %v, expected :%v", nodeA.Risk.GetGeneralRate(), expectedGeneralRate)
	}

	if nodeA.Risk.GetSendRate() != expectedSendRate {
		t.Fatalf("unexpected send rate: %v, expected :%v", nodeA.Risk.GetSendRate(), expectedSendRate)
	}

	if nodeA.Risk.GetReceiveRate() != expectedReceiveRate {
		t.Fatalf("unexpected receive rate: %v, expected :%v", nodeA.Risk.GetReceiveRate(), expectedReceiveRate)
	}
}

func NewTx(hash, from, to string, value int64) *database.Transaction {
	return &database.Transaction{
		Hash:        hash,
		FromAddress: from,
		ToAddress:   to,
		Value:       big.NewInt(value),
	}
}
