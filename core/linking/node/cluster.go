package node

import (
	"context"
	"fmt"
	"math/big"

	"aml/core/database"
	"aml/core/linking/node/rate"
	"aml/core/linking/prices"
	"aml/di"
)

type Cluster struct {
	name string

	Children map[string]*SingleNode

	Risk *rate.Risk
}

func (cluster *Cluster) CopyByCurrency(storage *NodesStorage, currency string) Node {
	copyCluster := &Cluster{
		name:     cluster.name,
		Children: make(map[string]*SingleNode),
	}

	for address, child := range cluster.Children {
		copyChild := NewSingleNode(child.Account, storage)
		storage.Add(address, copyCluster)
		copyCluster.Children[address] = child.copyByCurrency(copyChild, storage, currency)
	}

	storage.Add(cluster.name, copyCluster)

	for child := range cluster.Children { //todo in first add bug with different pointer on root
		storage.Add(child, copyCluster)
	}

	return copyCluster
}

func NewCluster(ctx context.Context, address string, cache *NodesStorage) (*Cluster, error) {
	entity, err := getEntity(ctx, address)
	if err != nil {
		return nil, err
	}

	root := &Cluster{
		name:     entity.Name,
		Children: make(map[string]*SingleNode),
	}

	for _, child := range entity.Accounts {
		acc, err := GetAccount(ctx, child.Address)
		if err != nil {
			return nil, err
		}

		clusterAccNode := NewSingleNode(acc, cache)

		root.Children[child.Address] = clusterAccNode
		cache.Add(acc.Address, root)
	}

	return root, nil
}

func (cluster *Cluster) GetName() string {
	return cluster.name
}

func (cluster *Cluster) GetCluster() (*Cluster, bool) {
	return cluster, true
}

func (cluster *Cluster) Link(ctx context.Context, depth int) error {
	if len(cluster.Children) > 1000 || depth > 3 {
		return nil
	}

	fmt.Println("LINK CLUSTER", len(cluster.Children))
	for _, child := range cluster.Children {
		err := cluster.linkChild(ctx, child, depth)
		if err != nil {
			return err
		}
	}

	return nil
}

func (cluster *Cluster) GetLinks() map[string]struct{} {
	links := make(map[string]struct{})

	for address := range cluster.Children {
		links[address] = struct{}{}
	}

	return links
}

func (cluster *Cluster) AddTx(address string, tx *database.Transaction) {
	childAddr := GetPartnerAddress(address, tx)
	cluster.Children[childAddr].AddTx(address, tx)
}

func (cluster *Cluster) GetRisk() *rate.Risk {
	return cluster.Risk
}

func (cluster *Cluster) IsAccountTypeToStop() bool {
	return false
}

func (cluster *Cluster) linkChild(ctx context.Context, child *SingleNode, depth int) error {
	if child.IsAccountTypeToStop() {
		return nil
	}

	txs, err := child.GetTransactions(ctx)
	if err != nil {
		return err
	}

	if len(txs) > 2000 {
		return nil
	}

	fmt.Println("START CLUSTER CHILD LINK", len(txs))
	for _, tx := range txs {
		if cluster.isSelfTx(tx) {
			anotherChild := GetPartnerAddress(child.Account.Address, tx)
			child.AddTx(anotherChild, tx)
			cluster.GetNode(anotherChild).AddTx(child.Account.Address, tx)

			continue
		}

		err := child.ProcessTx(ctx, tx, depth)
		if err != nil {
			return err
		}
	}

	return nil
}

func (cluster *Cluster) isSelfTx(tx *database.Transaction) bool {
	_, isClusterFrom := cluster.Children[tx.FromAddress]
	_, isClusterTo := cluster.Children[tx.ToAddress]

	return isClusterFrom && isClusterTo
}

func getEntity(ctx context.Context, address string) (entity *database.Entity, err error) {
	err = di.FromContext(ctx).Invoke(func(db database.Database) (innerErr error) {
		entity, innerErr = db.GetEntity(ctx, address)

		return innerErr
	})

	return
}

func (cluster *Cluster) GetTxsByAddress(address string) map[string]database.Transactions {
	return cluster.Children[address].GetTxsByAddress(address)
}

func (cluster *Cluster) GetTxs() database.Transactions {
	var clusterTxs database.Transactions

	for _, child := range cluster.Children {
		clusterTxs = append(clusterTxs, child.GetTxs()...)
	}

	return clusterTxs
}

func (cluster *Cluster) GetNode(address string) *SingleNode {
	if cluster.name == address {
		clusterNode := &SingleNode{
			Account: &database.Account{
				Address: cluster.name,
				AccType: -1,
			},
			Links: map[string]struct{}{},
			Risk:  cluster.Risk,
		}

		for child := range cluster.Children {
			clusterNode.Links[child] = struct{}{}
		}

		return clusterNode
	}

	return cluster.Children[address]
}

func (cluster *Cluster) CalculateRisk(branch map[string]int, curPrices *prices.Prices, risksByTypes map[database.AccountType]int) {
	if cluster.Risk != nil && cluster.Risk.IsCalculatedEdges(branch) {
		return
	}

	nodeRate := rate.New(branch)

	if branch == nil {
		branch = make(map[string]int)
	}

	branch[cluster.name] = len(branch)
	defer delete(branch, cluster.name)

	for _, child := range cluster.Children {
		child.CalculateRisk(branch, curPrices, risksByTypes)

		selfReceived, selfSent := cluster.GetSumSelfTxs(child, curPrices)

		nodeRate.Send.Money += child.GetRisk().Send.Money - selfSent
		nodeRate.Receive.Money += child.GetRisk().Receive.Money - selfReceived
	}

	for _, child := range cluster.Children {
		childRisk := child.GetRisk()

		selfReceived, selfSent := cluster.GetSumSelfTxs(child, curPrices)

		nodeRate.CalculateReceiveRisk(childRisk.Receive.Risk, childRisk.Receive.Money-selfReceived, childRisk.Receive.ByTypes, nodeRate.Receive.Money)
		nodeRate.CalculateSendRisk(childRisk.Send.Risk, childRisk.Send.Money-selfSent, childRisk.Send.ByTypes, nodeRate.Send.Money)
	}

	if cluster.Risk != nil {
		cluster.Risk.CalculateFromRate(nodeRate)

		return
	}

	cluster.Risk = nodeRate
}

func (cluster *Cluster) GetSumSelfTxs(child *SingleNode, curPrices *prices.Prices) (received, sent float64) {
	for _, tx := range child.GetTxs() {
		if !cluster.isSelfTx(tx) {
			continue
		}

		valueInUSD := float64(tx.Value.Int64()) * curPrices.Get(prices.GetCurrency(tx))

		if tx.FromAddress == child.Account.Address {
			sent += valueInUSD
		} else {
			received += valueInUSD
		}
	}

	return
}

func (cluster *Cluster) CalculateTurnoverByAddress(address string, curPrices *prices.Prices) (inTurnover float64, outTurnover float64) {
	for _, child := range cluster.Children {
		in, out := child.CalculateTurnoverByAddress(address, curPrices)

		inTurnover += in
		outTurnover += out
	}

	return
}

func (cluster *Cluster) GetBalanceByCurrency() (balance map[string]*big.Int, sent map[string]*big.Int, received map[string]*big.Int) {
	balance = make(map[string]*big.Int)
	sent = make(map[string]*big.Int)
	received = make(map[string]*big.Int)

	for _, child := range cluster.Children {
		childBalance, childSent, childReceived := child.GetBalanceByCurrency()

		addCurrency(balance, childBalance)
		addCurrency(sent, childSent)
		addCurrency(received, childReceived)
	}

	return
}

func addCurrency(original, child map[string]*big.Int) {
	for currency, value := range child {
		original[currency] = big.NewInt(0).Add(original[currency], value)
	}
}
