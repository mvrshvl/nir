package node

import (
	"context"
	"fmt"
	"math/big"
	"sort"

	"aml/core/database"
	"aml/core/linking/node/rate"
	"aml/core/linking/prices"
	"aml/di"
)

type SingleNode struct {
	Account *database.Account
	Txs     map[string]database.Transactions
	Links   map[string]struct{}
	storage *NodesStorage
	Risk    *rate.Risk
}

func NewSingleNode(acc *database.Account, storage *NodesStorage) *SingleNode {
	node := &SingleNode{
		Account: acc,
		Txs:     make(map[string]database.Transactions),
		Links:   make(map[string]struct{}),
		storage: storage,
	}

	return node
}

func (n *SingleNode) GetName() string {
	return n.Account.Address
}

func (n *SingleNode) GetAccountType() database.AccountType {
	return n.Account.AccType
}

func (n *SingleNode) GetCluster() (*Cluster, bool) {
	return nil, false
}

func (n *SingleNode) GetRisk() *rate.Risk {
	return n.Risk
}

func (n *SingleNode) GetLinks() map[string]struct{} {
	return n.Links
}

func (n *SingleNode) Link(ctx context.Context, depth int) error {
	txs, err := n.GetTransactions(ctx)
	if err != nil {
		return err
	}

	if len(txs) > 2000 || depth > 2 {
		fmt.Println("DEPTH", depth)
		return nil
	}

	if n.IsAccountTypeToStop() {
		return nil
	}

	fmt.Println("START LINK", len(txs), n.Account.Address)
	for i, tx := range txs {
		fmt.Println("Process LINK", i, n.Account.Address)
		if i > 100 {
			fmt.Println("STOP LINK", len(txs))
			break
		}

		err := n.ProcessTx(ctx, tx, depth)
		if err != nil {
			return err
		}
	}

	return nil
}

func (n *SingleNode) ProcessTx(ctx context.Context, tx *database.Transaction, depth int) error {
	address := GetPartnerAddress(n.Account.Address, tx)

	if len(address) == 0 {
		return nil
	}

	if n.Account.Address == address {
		return nil
	}

	if _, ok := n.Links[address]; ok { // address is already exist in links
		n.storage.Get(address).AddTx(n.Account.Address, tx)
		n.AddTx(address, tx)

		return nil
	}

	cachedNode := n.storage.Get(address)
	if cachedNode != nil {
		n.addLink(address, tx)
		cachedNode.GetNode(address).addLink(n.Account.Address, tx)

		return nil
	}

	child, err := New(ctx, address, n.storage)
	if err != nil {
		return nil
	}

	n.addLink(address, tx)
	child.GetNode(address).addLink(n.Account.Address, tx)

	return child.Link(ctx, depth+1)
}

func (n *SingleNode) addLink(address string, tx *database.Transaction) {
	n.Links[address] = struct{}{}
	n.AddTx(address, tx)
}

func (n *SingleNode) AddTx(address string, tx *database.Transaction) {
	for _, existedTx := range n.Txs[address] {
		if existedTx.Hash == tx.Hash {
			return
		}
	}

	n.Txs[address] = append(n.Txs[address], tx)
}

func (n *SingleNode) GetTransactions(ctx context.Context) (txs database.Transactions, err error) {
	err = di.FromContext(ctx).Invoke(func(db database.Database) (innerErr error) {
		txs, innerErr = db.GetTransactionsByAddress(ctx, n.Account.Address)
		if innerErr != nil {
			return innerErr
		}

		return nil
	})

	return
}

func (n *SingleNode) GetType() database.AccountType {
	return n.Account.AccType
}

func (n *SingleNode) IsAccountTypeToStop() bool {
	if len(n.storage.Storage) <= 1 { // if its requested address
		return false
	}

	switch n.Account.AccType {
	case database.EOA, database.Deposit:
		return false
	default:
		return true
	}
}

func (n *SingleNode) GetTxsByAddress(address string) map[string]database.Transactions {
	return n.Txs
}

func (n *SingleNode) GetTxs() database.Transactions {
	var nodeTxs database.Transactions

	for _, txs := range n.Txs {
		nodeTxs = append(nodeTxs, txs...)
	}

	return nodeTxs
}

func (n *SingleNode) GetNode(_ string) *SingleNode {
	return n
}

func GetPartnerAddress(address string, transaction *database.Transaction) string {
	partner := transaction.FromAddress // incoming tx

	if partner == address {
		partner = transaction.ToAddress // outgoing tx
	}

	return partner
}

func (n *SingleNode) CalculateRisk(branch map[string]int, curPrices *prices.Prices, risksByTypes map[database.AccountType]int) {
	if n.Risk != nil && n.Risk.IsCalculatedEdges(branch) {
		return
	}

	nodeRate := rate.New(branch)

	if n.IsAccountTypeToStop() && len(branch) > 1 {
		n.Risk = nodeRate

		risk := float64(risksByTypes[n.Account.AccType])

		n.Risk.Send = rate.DirectedRisk{
			Risk:    risk,
			ByTypes: map[string]float64{n.Account.AccType.GetDescription(): 100},
		}

		n.Risk.Receive = rate.DirectedRisk{
			Risk:    risk,
			ByTypes: map[string]float64{n.Account.AccType.GetDescription(): 100},
		}

		n.Risk.Receive.Money, n.Risk.Send.Money, _ = n.calculateTurnover(branch, curPrices)

		return
	}

	if branch == nil {
		branch = make(map[string]int)
	}

	branch[n.Account.Address] = len(branch)
	defer delete(branch, n.Account.Address)

	var sortedLinks []*linkWithMoney

	nodeRate.Receive.Money, nodeRate.Send.Money, sortedLinks = n.calculateTurnover(branch, curPrices)
	for _, link := range sortedLinks {
		linkNode := n.storage.Get(link.address)
		if _, ok := branch[linkNode.GetName()]; ok {
			continue
		}

		linkNode.CalculateRisk(branch, curPrices, risksByTypes)

		sentToLinkNode, receivedFromLinkNode := linkNode.CalculateTurnoverByAddress(n.Account.Address, curPrices)

		linkNodeRisk := linkNode.GetRisk()
		nodeRate.CalculateSendRisk(linkNodeRisk.Send.Risk, sentToLinkNode, linkNodeRisk.Send.ByTypes, nodeRate.Send.Money)
		nodeRate.CalculateReceiveRisk(linkNodeRisk.Receive.Risk, receivedFromLinkNode, linkNodeRisk.Receive.ByTypes, nodeRate.Receive.Money)
	}

	if n.Risk != nil {
		n.Risk.CalculateFromRate(nodeRate)

		return
	}

	n.Risk = nodeRate
}

type linkWithMoney struct {
	address string
	money   float64
}

func (n *SingleNode) calculateTurnover(branch map[string]int, curPrices *prices.Prices) (received float64, sent float64, sortedLinks []*linkWithMoney) {
	for address := range n.Txs {
		if _, ok := branch[address]; ok {
			continue
		}

		receivedFromAddr, sentToAddr := n.CalculateTurnoverByAddress(address, curPrices)

		received += receivedFromAddr
		sent += sentToAddr

		sortedLinks = append(sortedLinks, &linkWithMoney{
			address: address,
			money:   receivedFromAddr + sentToAddr,
		})
	}

	sort.Slice(sortedLinks, func(i, j int) bool {
		return sortedLinks[i].money < sortedLinks[j].money
	})

	return
}

func (n *SingleNode) CalculateTurnoverByAddress(address string, curPrices *prices.Prices) (received float64, sent float64) {
	txs := n.Txs[address]

	for _, tx := range txs {
		value := float64(tx.Value.Uint64())

		if value <= 0 {
			continue
		}

		valueInUSD := curPrices.GetValueInUSD(tx)

		if tx.FromAddress == n.Account.Address {
			sent += valueInUSD
		} else {
			received += valueInUSD
		}
	}

	return
}

func (n *SingleNode) CopyByCurrency(newStorage *NodesStorage, currency string) Node {
	copyNode := NewSingleNode(n.Account, newStorage)
	newStorage.Add(n.Account.Address, copyNode)

	return n.copyByCurrency(copyNode, newStorage, currency)
}

func (n *SingleNode) copyByCurrency(dst *SingleNode, newStorage *NodesStorage, currency string) *SingleNode {
	copyTxs := make(map[string]database.Transactions)

	for address, txs := range n.Txs {
		for _, tx := range txs {
			if prices.GetCurrency(tx) == currency {
				if _, ok := copyTxs[address]; !ok {
					copyTxs[address] = database.Transactions{tx}

					continue
				}

				copyTxs[address] = append(copyTxs[address], tx)
			}
		}
	}

	copyLinks := make(map[string]struct{})
	for address := range copyTxs {
		copyLinks[address] = struct{}{}

		if newStorage.Get(address) != nil {
			continue
		}

		node := n.storage.Get(address)
		node.CopyByCurrency(newStorage, currency)
	}

	dst.Account = n.Account
	dst.Links = copyLinks
	dst.Txs = copyTxs
	dst.storage = newStorage

	return dst
}

func (n *SingleNode) GetBalanceByCurrency() (balance map[string]*big.Int, sent map[string]*big.Int, received map[string]*big.Int) {
	sent = make(map[string]*big.Int)
	received = make(map[string]*big.Int)

	for _, txs := range n.Txs {
		for _, tx := range txs {
			currency := prices.GetCurrency(tx)

			if _, ok := sent[currency]; !ok {
				sent[currency] = big.NewInt(0)
			}

			if _, ok := received[currency]; !ok {
				received[currency] = big.NewInt(0)
			}

			if tx.FromAddress == n.Account.Address {
				sent[currency] = big.NewInt(0).Add(tx.Value, sent[currency])
			} else {
				received[currency] = big.NewInt(0).Add(tx.Value, received[currency])
			}
		}
	}

	balance = make(map[string]*big.Int)

	for currecy, money := range received {
		value := money

		if moneySent, ok := sent[currecy]; ok {
			fmt.Println("BALANCE", currecy, money.String(), moneySent.String())

			value = big.NewInt(0).Sub(value, moneySent)
		}

		balance[currecy] = value
	}

	return
}
