package prices

import (
	"aml/core/database"
	gecko "aml/core/linking/geckocoin"
	"aml/log"
	"context"
	"golang.org/x/time/rate"
	"time"
)

type Prices struct {
	Currency map[string]float64
}

func New(ctx context.Context, currencies map[string]struct{}) *Prices {
	prices := &Prices{
		Currency: make(map[string]float64),
	}

	geckoClient := gecko.NewClient()

	limiter := rate.NewLimiter(rate.Every(time.Minute/50), 1)

	log.Errorf(ctx, "prices count", len(currencies))

	for currency := range currencies {
		limiter.Wait(ctx)
		price, err := geckoClient.GetPrice(currency)
		if err != nil {
			log.Debugf(ctx, "can't get price of %s: %v", currency, err)
			continue
		}

		prices.add(currency, price)
	}

	return prices
}

func (p *Prices) Get(currency string) float64 {
	price, ok := p.Currency[currency]
	if !ok {
		return 0
	}

	return price
}

func (p *Prices) add(currency string, price float64) {
	p.Currency[currency] = price
}

func GetCurrency(tx *database.Transaction) string {
	currency := gecko.EthCurrency

	if tx.ContractAddress != nil {
		currency = *tx.ContractAddress
	}

	return currency
}

func (p *Prices) GetValueInUSD(tx *database.Transaction) float64 {
	currency := GetCurrency(tx)
	price := p.Get(currency)

	if currency == gecko.EthCurrency {
		return float64(tx.Value.Uint64()) / gecko.WeiInEth * price
	}

	return float64(tx.Value.Uint64()) * price
}
