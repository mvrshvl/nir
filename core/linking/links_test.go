package linking

import (
	"context"
	"fmt"
	"math/big"
	"reflect"
	"sort"
	"testing"

	"github.com/golang/mock/gomock"

	"aml/core/database"
	"aml/di"
)

func TestRoot_Link(t *testing.T) {
	ctrl := gomock.NewController(t)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	mockDB := database.NewMockDatabase(ctrl)

	container, err := di.BuildContainer(
		func() database.Database {
			return mockDB
		})
	if err != nil {
		t.Fatal(err)
	}

	ctx = di.WithContext(ctx, container)

	// A [EOA] 100 -> C [EOA] 50 -> D [Ransom] 50 -> A
	// C 50 -> B [EOA]
	// B -> 25 -> E [Deposit] 25 -> Ex [Exchange]
	// B -> 25 -> F [DarkService]
	// cluster {B, C}

	const (
		addressA        = "A"
		addressB        = "B"
		addressC        = "C"
		addressD        = "D"
		addressE        = "E"
		addressF        = "F"
		addressExchange = "EXCHANGE"
		clusterBC       = "cluster BC"
	)

	txAToC := &database.Transaction{
		Hash:        "txAToC",
		FromAddress: addressA,
		ToAddress:   addressC,
		Value:       big.NewInt(100),
	}

	txDToA := &database.Transaction{
		Hash:        "txDToA",
		FromAddress: addressD,
		ToAddress:   addressA,
		Value:       big.NewInt(10),
	}

	txCToB := &database.Transaction{
		Hash:        "txCToC",
		FromAddress: addressC,
		ToAddress:   addressB,
		Value:       big.NewInt(50),
	}

	txCToD := &database.Transaction{
		Hash:        "txCToD",
		FromAddress: addressC,
		ToAddress:   addressD,
		Value:       big.NewInt(50),
	}

	txBToE := &database.Transaction{
		Hash:        "txBToE",
		FromAddress: addressB,
		ToAddress:   addressE,
		Value:       big.NewInt(25),
	}

	txBToF := &database.Transaction{
		Hash:        "txBToF",
		FromAddress: addressB,
		ToAddress:   addressF,
		Value:       big.NewInt(25),
	}

	txEToExchange := &database.Transaction{
		Hash:        "txEToExch",
		FromAddress: addressE,
		ToAddress:   addressExchange,
		Value:       big.NewInt(25),
	}

	addresses := []string{
		addressA, addressB, addressC, addressD, addressE, addressF, addressE, addressExchange,
	}

	numCluster := uint64(1)

	accountB := &database.Account{Address: addressB, AccType: database.EOA, Cluster: &numCluster}
	accountC := &database.Account{Address: addressC, AccType: database.EOA, Cluster: &numCluster}
	accountA := &database.Account{Address: addressA, AccType: database.EOA}
	accountD := &database.Account{Address: addressD, AccType: database.Ransom}
	accountE := &database.Account{Address: addressE, AccType: database.Deposit}
	accountExchange := &database.Account{Address: addressExchange, AccType: database.ExchangeLR}
	accountF := &database.Account{Address: addressF, AccType: database.DarkService}

	for _, address := range addresses {
		mockDB.EXPECT().GetAccount(ctx, addressA).Return(accountA, nil).AnyTimes()
		mockDB.EXPECT().GetAccount(ctx, addressC).Return(accountC, nil).AnyTimes()
		mockDB.EXPECT().GetAccount(ctx, addressB).Return(accountB, nil).AnyTimes()
		mockDB.EXPECT().GetAccount(ctx, addressD).Return(accountD, nil).AnyTimes()
		mockDB.EXPECT().GetAccount(ctx, addressE).Return(accountE, nil).AnyTimes()
		mockDB.EXPECT().GetAccount(ctx, addressExchange).Return(accountExchange, nil).AnyTimes()
		mockDB.EXPECT().GetAccount(ctx, addressF).Return(accountF, nil).AnyTimes()

		mockDB.EXPECT().GetTransactionsByAddress(ctx, addressA).Return(database.Transactions{txAToC, txDToA}, nil)
		mockDB.EXPECT().GetTransactionsByAddress(ctx, addressB).Return(database.Transactions{txBToE, txBToF, txCToB}, nil)
		mockDB.EXPECT().GetTransactionsByAddress(ctx, addressC).Return(database.Transactions{txAToC, txCToB, txCToD}, nil)
		mockDB.EXPECT().GetTransactionsByAddress(ctx, addressD).Return(database.Transactions{txDToA, txCToD}, nil)
		mockDB.EXPECT().GetTransactionsByAddress(ctx, addressE).Return(database.Transactions{txBToE, txEToExchange}, nil)
		mockDB.EXPECT().GetTransactionsByAddress(ctx, addressF).Return(database.Transactions{txBToF}, nil)
		mockDB.EXPECT().GetTransactionsByAddress(ctx, addressExchange).Return(database.Transactions{txEToExchange}, nil)

		mockDB.EXPECT().GetEntity(ctx, gomock.Any()).Return(&database.Entity{Name: clusterBC, Accounts: []*database.Account{accountB, accountC}}, nil)

		links, err := Run(ctx, address)
		if err != nil {
			t.Fatal(err)
		}

		if !reflect.DeepEqual(links.Nodes.Get(addressA).GetLinks(), map[string]struct{}{addressC: {}, addressD: {}}) ||
			!reflect.DeepEqual(sortTxs(links.Nodes.Get(addressA).GetTxs()), database.Transactions{txAToC, txDToA}) {
			t.Fatalf("incorrect root state")
		}

		clusterLinks := map[string]struct{}{addressB: {}, addressC: {}}
		if !reflect.DeepEqual(links.Nodes.Get(addressB).GetLinks(), clusterLinks) || !reflect.DeepEqual(links.Nodes.Get(addressC).GetLinks(), clusterLinks) {
			t.Fatalf("incorrect cluster children")
		}

		if !reflect.DeepEqual(links.Nodes.Get(addressB).GetNode(addressB).GetLinks(), map[string]struct{}{addressE: {}, addressF: {}}) ||
			!reflect.DeepEqual(sortTxs(links.Nodes.Get(addressB).GetNode(addressB).GetTxs()), database.Transactions{txBToE, txBToF, txCToB}) {
			t.Fatal("incorrect state for cluster child B")
		}

		if !reflect.DeepEqual(links.Nodes.Get(addressC).GetNode(addressC).GetLinks(), map[string]struct{}{addressA: {}, addressD: {}}) ||
			!reflect.DeepEqual(sortTxs(links.Nodes.Get(addressC).GetNode(addressC).GetTxs()), database.Transactions{txAToC, txCToB, txCToD}) {
			t.Fatal("incorrect state for cluster child C")
		}

		if !reflect.DeepEqual(links.Nodes.Get(addressD).GetLinks(), map[string]struct{}{addressC: {}, addressA: {}}) ||
			!reflect.DeepEqual(sortTxs(links.Nodes.Get(addressD).GetTxs()), database.Transactions{txCToD, txDToA}) {
			t.Fatalf("incorrect state for account D")
		}

		if !reflect.DeepEqual(links.Nodes.Get(addressE).GetLinks(), map[string]struct{}{addressB: {}, addressExchange: {}}) ||
			!reflect.DeepEqual(sortTxs(links.Nodes.Get(addressE).GetTxs()), database.Transactions{txBToE, txEToExchange}) {
			t.Fatalf("incorrect state for deposit")
		}

		if !reflect.DeepEqual(links.Nodes.Get(addressExchange).GetLinks(), map[string]struct{}{addressE: {}}) ||
			!reflect.DeepEqual(sortTxs(links.Nodes.Get(addressExchange).GetTxs()), database.Transactions{txEToExchange}) {
			t.Fatalf("incorrect state for deposit")
		}

		if !reflect.DeepEqual(links.Nodes.Get(addressF).GetLinks(), map[string]struct{}{addressB: {}}) ||
			!reflect.DeepEqual(sortTxs(links.Nodes.Get(addressF).GetTxs()), database.Transactions{txBToF}) {
			t.Fatal("incorrect state for node F")
		}

		mockDB.EXPECT().GetAccountTypesRisks(ctx).Return(map[database.AccountType]int{
			database.EOA:         0,
			database.Scam:        100,
			database.Ransom:      50,
			database.ExchangeLR:  10,
			database.DarkService: 80,
			database.Deposit:     0,
		}, nil)

		err = links.CalculateRisks(ctx)
		if err != nil {
			t.Fatal(err)
		}

		fmt.Println(links.links.GetRisk().GetGeneralRate(), links.links.GetRisk().Send.Risk, links.links.GetRisk().Receive.Risk)
	}
}

func sortTxs(transactions database.Transactions) database.Transactions {
	sort.Slice(transactions, func(i, j int) bool {
		return transactions[i].Hash < transactions[j].Hash
	})

	return transactions
}
