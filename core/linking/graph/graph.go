package graph

import (
	"bytes"
	"fmt"
	"io"

	"github.com/go-echarts/go-echarts/v2/charts"
	"github.com/go-echarts/go-echarts/v2/components"
	"github.com/go-echarts/go-echarts/v2/opts"

	"aml/amlerror"
	"aml/core/linking"
	"aml/core/linking/node"
	"aml/core/linking/node/rate"
)

const (
	cluster    = 4
	highRate   = 1
	mediumRate = 2
	lowRate    = 3
	requested  = 5
)

type Description struct {
	SendRisk    float64
	ReceiveRisk float64
}

type category struct {
	name  string
	color string
}

func getNodeTypes() map[int]category {
	return map[int]category{
		highRate: {
			name:  "low risk",
			color: "green",
		},
		mediumRate: {
			name:  "medium risk",
			color: "orange",
		},
		lowRate: {
			name:  "high risk",
			color: "red",
		},
		requested: {
			name:  "requested",
			color: "black",
		},
		cluster: {
			name:  "cluster",
			color: "blue",
		},
	}
}

func getCategories() (categories []*opts.GraphCategory) {
	nodeTypes := getNodeTypes()

	for i := 1; i <= len(nodeTypes)+1; i++ {
		categories = append(categories, &opts.GraphCategory{
			Name: nodeTypes[i-1].name,
			Label: &opts.Label{
				Show:  false,
				Color: nodeTypes[i].color,
			},
		})
	}

	return
}

func newNode(name string, category int, nodeType float32) opts.GraphNode {
	size := 20

	if category == requested {
		size *= 2
	}

	return opts.GraphNode{
		Name:       name,
		Category:   category,
		SymbolSize: size,
		Value:      nodeType,
		ItemStyle: &opts.ItemStyle{
			Opacity: 0,
		},
	}
}

type Graph struct {
	mainAddress string
	nodes       map[string]opts.GraphNode
	links       []opts.GraphLink
	depth       int
}

func newGraph(depth int) *Graph {
	return &Graph{
		nodes: make(map[string]opts.GraphNode),
		links: make([]opts.GraphLink, 0),
		depth: depth,
	}
}

func Run(links *linking.Links, mainAddress string, depth, nodeMaxCountLinks int) (*bytes.Buffer, error) {
	graph := newGraph(depth)

	graph.mainAddress = mainAddress

	graph.link(links.GetNodes().Get(mainAddress), links.GetNodes(), make(map[string]struct{}), nodeMaxCountLinks)

	return graph.generate(links.GetNodes())
}

func (g *Graph) link(node node.Node, nodes *node.NodesStorage, branch map[string]struct{}, nodeMaxCountLinks int) {
	g.addNode(node)

	links := node.GetLinks()
	if len(branch) > g.depth || len(links) > nodeMaxCountLinks && len(branch) > 1 {
		return
	}

	branch[node.GetName()] = struct{}{}

	for link := range node.GetLinks() {
		g.addLink(node.GetName(), link)

		if _, ok := branch[link]; ok {
			continue
		}

		if _, ok := g.nodes[link]; ok {
			continue
		}

		linkNode := nodes.Get(link).GetNode(link)
		g.link(linkNode, nodes, branch, nodeMaxCountLinks)
	}

	delete(branch, node.GetName())
}

func (g *Graph) addNode(node node.Node) {
	address := node.GetName()

	_, ok := g.nodes[address]
	if ok {
		return
	}

	singleNode := node.GetNode(address)

	g.nodes[address] = newNode(address, g.getNodeCategory(singleNode), float32(singleNode.GetAccountType()))
}

func getDescription(node *node.SingleNode) string {
	return fmt.Sprintf("Address: %v;Account type: %v;Rating: %.2f;Send transaction risk: %.2f;Receive transaction risk: %.2f",
		node.GetName(), node.GetAccountType().GetDescription(), node.GetRisk().GetGeneralRate(), node.GetRisk().GetSendRisk(), node.GetRisk().GetReceiveRisk())
}

func (g *Graph) addLink(from, to string) {
	g.links = append(g.links, opts.GraphLink{
		Source: from,
		Target: to,
	})
}

func (g *Graph) getNodeCategory(node *node.SingleNode) int {
	if node.GetAccountType() == -1 {
		return cluster
	}

	if node.GetName() == g.mainAddress {
		return requested
	}

	nodeRate := node.GetRisk().GetGeneralRate()
	if nodeRate > rate.MediumRate {
		return highRate
	} else if nodeRate > rate.LowRate {
		return mediumRate
	}

	return lowRate
}

func (g Graph) generate(nodesStorage *node.NodesStorage) (*bytes.Buffer, error) {
	nodes := make([]opts.GraphNode, 0)

	for i := range g.links {
		err := g.rename(&g.links[i], nodesStorage)
		if err != nil {
			return nil, err
		}
	}

	for _, node := range g.nodes {
		nodes = append(nodes, node)
	}

	graph := charts.NewGraph()
	graph.SetGlobalOptions(
		charts.WithLegendOpts(opts.Legend{Show: true}),
		charts.WithInitializationOpts(opts.Initialization{
			Width:  "100%",
			Height: "1000%",
		}),
		charts.WithTitleOpts(opts.Title{Title: "basic graph example"}),
	)

	graph.AddSeries("Ethereum account", nodes, g.links,
		charts.WithGraphChartOpts(
			opts.GraphChart{Roam: true, Force: &opts.GraphForce{Repulsion: 1000}, FocusNodeAdjacency: true, Categories: getCategories()},
		),
		charts.WithLabelOpts(opts.Label{Show: false, Formatter: " "}),
	)

	graph.SetGlobalOptions(charts.WithToolboxOpts(opts.Toolbox{
		Show: true,
		Feature: &opts.ToolBoxFeature{
			SaveAsImage: &opts.ToolBoxFeatureSaveAsImage{
				Show:  true,
				Type:  "png",
				Name:  "graph",
				Title: "save",
			},
		},
	}), charts.WithTooltipOpts(opts.Tooltip{
		Show:      true,
		Trigger:   "item",
		TriggerOn: "click",
		Formatter: opts.FuncOpts(`function (params, ticket, callback) {
	const data = params.name.split(';');
    return data[0] + '<br/>' + data[1] + '<br/>' + data[2] + '<br/>' + data[3] + '<br/>' + data[4];
}`),
		AxisPointer: nil,
	}))

	page := components.NewPage()

	page.AddCharts(
		graph,
	)

	b := new(bytes.Buffer)

	err := page.Render(io.MultiWriter(b))

	return b, err
}

const errNodeMissing = amlerror.AMLError("node is missing")

func (g *Graph) rename(link *opts.GraphLink, nodes *node.NodesStorage) error {
	src := fmt.Sprint(link.Source)
	dst := fmt.Sprint(link.Target)

	srcNode, okSrc := g.nodes[src]
	dstNode, okDst := g.nodes[dst]

	if !okSrc || !okDst {
		return errNodeMissing
	}

	srcNode.Name = getDescription(nodes.Get(src).GetNode(src))
	dstNode.Name = getDescription(nodes.Get(dst).GetNode(dst))

	link.Source = srcNode.Name
	link.Target = dstNode.Name

	g.nodes[src] = srcNode
	g.nodes[dst] = dstNode

	return nil
}
