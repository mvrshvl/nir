package clustering

import (
	"aml/core/clustering/airdrop"
	logging "aml/log"
	"context"
	"sort"
	"sync"

	"aml/core/clustering/depositreuse"
	"aml/core/clustering/selfauth"
	"aml/core/database"
)

func Run(ctx context.Context, subscriber <-chan *database.NewBlocks) {
	for {
		select {
		case <-ctx.Done():
			return
		case newBlocks := <-subscriber:
			clustering(ctx, newBlocks)
		}
	}
}

func clustering(ctx context.Context, newBlocks *database.NewBlocks) {
	sort.Slice(newBlocks.Blocks, func(i, j int) bool {
		return newBlocks.Blocks[i].Number < newBlocks.Blocks[j].Number
	})

	fromBlock := newBlocks.Blocks[0].Number
	toBlock := newBlocks.Blocks[len(newBlocks.Blocks)-1].Number

	var wg sync.WaitGroup

	wg.Add(3)

	go func() {
		defer wg.Done()

		err := depositreuse.Run(ctx, fromBlock, toBlock)
		if err != nil {
			logging.Errorf(ctx, "can't clustering deposit reuse: %v", err)
		}
	}()

	go func() {
		defer wg.Done()

		err := airdrop.Run(ctx, toBlock)
		if err != nil {
			logging.Errorf(ctx, "can't clustering airdrop: %v", err)
		}
	}()

	go func() {
		defer wg.Done()

		err := selfauth.Run(ctx, toBlock)
		if err != nil {
			logging.Errorf(ctx, "can't clustering selfauth: %v", err)
		}
	}()

	wg.Wait()
}
