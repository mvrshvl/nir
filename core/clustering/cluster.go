package clustering

import (
	"sync"

	"aml/core/clustering/transfer"
	"aml/core/database"
)

type Cluster struct {
	Accounts                  map[string]struct{}
	AccountsExchangeTransfers map[string][]*database.ExchangeTransfer
	AccountsTokenTransfers    map[string][]*transfer.TokenTransfer
	TokensAuth                map[string]map[string]*database.ERC20Approve

	mux sync.RWMutex
}

type Clusters []*Cluster

func (cl *Cluster) Merge(cluster *Cluster) bool {
	cl.mux.Lock()
	defer cl.mux.Unlock()

	cluster.mux.RLock()
	defer cluster.mux.RUnlock()

	for acc := range cluster.Accounts {
		if _, ok := cl.Accounts[acc]; ok {
			cl.merge(cluster)

			return true
		}
	}

	return false
}

func (cl *Cluster) merge(cluster *Cluster) {
	for acc := range cluster.Accounts {
		cl.Accounts[acc] = struct{}{}

		if exchangeTransfers, ok := cluster.AccountsExchangeTransfers[acc]; ok {
			cl.AccountsExchangeTransfers[acc] = append(cl.AccountsExchangeTransfers[acc], exchangeTransfers...)
		}

		if tokenTransfers, ok := cluster.AccountsTokenTransfers[acc]; ok {
			cl.AccountsTokenTransfers[acc] = append(cl.AccountsTokenTransfers[acc], tokenTransfers...)
		}
	}

	for token, approves := range cluster.TokensAuth {
		if _, ok := cl.TokensAuth[token]; !ok {
			cl.TokensAuth[token] = approves

			continue
		}

		for _, approve := range approves {
			cl.TokensAuth[token][approve.TxHash] = approve
		}
	}
}
