package user

import (
	"context"
	"math/big"
	"math/rand"
	"os"
	"time"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/gocarina/gocsv"

	"aml/core/database"
	"aml/geth"
	"aml/test/contract"
	"aml/test/entity/account"
	"aml/test/exchange"
	"aml/test/writer"
)

type User struct {
	accounts []*account.Account
	deposits map[string][]common.Address
}

type Users []*User

type transferToken func(ctx context.Context, from *account.Account, toAddr common.Address, amount *big.Int) error

func CreateUserFromSize(size uint64) (*User, error) {
	user := &User{
		accounts: make([]*account.Account, size),
		deposits: make(map[string][]common.Address),
	}

	for i := uint64(0); i < size; i++ {
		acc, err := account.CreateAccount()
		if err != nil {
			return nil, err
		}

		user.accounts[i] = acc
	}

	return user, nil
}

func (usr *User) SendTransactionToExchange(ctx context.Context, exchange string, amount int64) (*types.LegacyTx, error) {
	acc := usr.accounts[rand.Intn(len(usr.accounts))]
	deposit := usr.deposits[exchange][rand.Intn(len(usr.deposits[exchange]))]

	return acc.SendTransaction(ctx, deposit, amount, false)
}

func (usr *User) SendTransaction(ctx context.Context, address common.Address, amount int64) (*types.LegacyTx, error) {
	acc := usr.accounts[rand.Intn(len(usr.accounts))]

	return acc.SendTransaction(ctx, address, amount, false)
}

func (usr *User) CreateExchangeAccounts(exchange *exchange.Exchange) error {
	for _, acc := range usr.accounts {
		deposit, err := exchange.CreateAccountIfNotExist(acc.GetAddress())
		if err != nil {
			return err
		}

		usr.deposits[exchange.GetName()] = append(usr.deposits[exchange.GetName()], deposit)
	}

	return nil
}

func (usr *User) GetAccounts() (addresses []common.Address) {
	for _, acc := range usr.accounts {
		addresses = append(addresses, acc.GetAddress())
	}

	return addresses
}

func (usr *User) SendTxToExchange(ctx context.Context, exchange string, amount int64) (*types.LegacyTx, error) {
	acc := usr.accounts[rand.Intn(len(usr.accounts))]
	deposit := usr.deposits[exchange][rand.Intn(len(usr.deposits[exchange]))]

	return acc.SendTransaction(ctx, deposit, amount, false)
}

func (usr *User) CollectTokenOnOneAcc(ctx context.Context, mixDepth int, getBalance func(address common.Address) (*big.Int, error), transferToken transferToken) error {
	if len(usr.accounts) == 1 {
		return nil
	}

	balances, err := usr.getTokenBalances(getBalance)
	if err != nil {
		return err
	}

	mainAccount := usr.RandomAccount()

	err = usr.mixTokens(ctx, balances, mixDepth, transferToken)
	if err != nil {
		return err
	}

	for acc := range balances {
		if acc.GetAddress().String() == mainAccount.GetAddress().String() {
			continue
		}

		err := transferWithinCluster(ctx, balances, acc, mainAccount, transferToken)
		if err != nil {
			return err
		}
	}

	return nil
}

func (usr *User) mixTokens(ctx context.Context, balances map[*account.Account]int64, mixDepth int, transferToken transferToken) error {
	currentDepth := 0
	sum := int64(0)

	for _, balance := range balances {
		sum += balance
	}

	for currentDepth < mixDepth-1 {
		for source, balance := range balances {
			target := usr.RandomAccount()

			if source.GetAddress().String() == target.GetAddress().String() || balance == 0 {
				continue
			}

			err := transferWithinCluster(ctx, balances, source, target, transferToken)
			if err != nil {
				return err
			}
		}

		currentDepth++
	}

	return nil
}

func (usr *User) getTokenBalances(getBalance func(address common.Address) (*big.Int, error)) (map[*account.Account]int64, error) {
	balances := make(map[*account.Account]int64)

	for _, acc := range usr.accounts {
		balance, err := getBalance(acc.GetAddress())
		if err != nil {
			return nil, err
		}

		balances[acc] = balance.Int64()
	}

	return balances, nil
}

func transferWithinCluster(ctx context.Context, balances map[*account.Account]int64, source, target *account.Account, transferToken transferToken) error {
	if balances[source] == 0 {
		return nil
	}

	err := transferToken(ctx, source, target.GetAddress(), big.NewInt(balances[source]))
	if err != nil {
		return err
	}

	balances[target] += balances[source]
	balances[source] = 0

	return nil
}

func (usr *User) RandomAccount() *account.Account {
	if len(usr.accounts) == 1 {
		return usr.accounts[0]
	}

	return usr.accounts[rand.Intn(len(usr.accounts)-1)]
}

const balance = 100000000000

func (usr *User) DeployContract(ctx context.Context, tokens int) (*contract.SimpleToken, error) {
	distributor := usr.RandomAccount()

	bytecode, err := contract.BytecodeContract(contract.SimpleTokenBin, contract.SimpleTokenABI, "test", "t", big.NewInt(int64(tokens)))
	if err != nil {
		return nil, err
	}

	var gas uint64

	err = writer.Execute(ctx, func(w *writer.Writer) (innerErr error) {
		gas, innerErr = w.EstimateGas(ctx, distributor.GetAddress(), nil, bytecode)

		return innerErr
	})
	if err != nil {
		return nil, err
	}

	err = writer.Execute(ctx, func(w *writer.Writer) error {
		_, innerErr := w.BalanceAt(ctx, distributor.GetAddress())
		if innerErr != nil {
			return innerErr
		}

		return nil
	})
	if err != nil {
		return nil, err
	}

	var token *contract.SimpleToken

	err = executeToken(ctx, distributor, gas, func(auth *bind.TransactOpts, backend bind.ContractBackend) (tx *types.Transaction, innerErr error) {
		_, tx, token, innerErr = contract.DeploySimpleToken(auth, backend, "TestContract", "TC", big.NewInt(balance))

		return tx, innerErr
	})
	if err != nil {
		return nil, err
	}

	for _, acc := range usr.accounts {
		if distributor.GetAddress().String() == acc.GetAddress().String() {
			continue
		}

		err = executeToken(ctx, distributor, gas, func(auth *bind.TransactOpts, backend bind.ContractBackend) (*types.Transaction, error) {
			return token.Approve(auth, acc.GetAddress(), big.NewInt(1))
		})
		if err != nil {
			return nil, err
		}
	}

	return token, err
}

func executeToken(ctx context.Context, owner *account.Account, gas uint64, fn func(auth *bind.TransactOpts, backend bind.ContractBackend) (*types.Transaction, error)) error {
	tx, err := owner.ExecuteContract(ctx, gas, fn)
	if err != nil {
		return err
	}

	return writer.Execute(ctx, func(w *writer.Writer) error {
		return w.WaitTx(ctx, tx.Hash())
	})
}

func (usr *User) ExecuteContract(ctx context.Context, gasLimit uint64, fn func(auth *bind.TransactOpts, backend bind.ContractBackend) (*types.Transaction, error)) (tx *types.Transaction, err error) {
	acc := usr.RandomAccount()

	return acc.ExecuteContract(ctx, gasLimit, fn)
}

func (usr *User) getBlacklistAccounts() (list []geth.Account) {
	for _, acc := range usr.accounts {
		if acc.AccType != database.EOA {
			list = append(list, geth.Account{
				Address:     acc.GetAddress().String(),
				AccountType: int64(acc.AccType),
				Date:        time.Now().String(),
			})
		}
	}

	return
}

func (users Users) SaveBlacklist() error {
	var list []geth.Account

	for _, user := range users {
		userList := user.getBlacklistAccounts()

		list = append(list, userList...)
	}

	f, err := os.Create("geth/data/blacklist.csv")
	if err != nil {
		return err
	}

	err = gocsv.Marshal(list, f)
	if err != nil {
		return err
	}

	return nil
}
