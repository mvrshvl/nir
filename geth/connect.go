package geth

import (
	"context"
	"fmt"
	"math/big"

	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/ethereum/go-ethereum/rpc"

	"aml/config"
)

type Worker struct {
	connect *ethclient.Client
}

func New(cfg *config.Config) (*Worker, error) {
	ethDirect, err := rpc.Dial(cfg.Ethereum.Address)
	if err != nil {
		return nil, fmt.Errorf("dial error %w", err)
	}

	return &Worker{connect: ethclient.NewClient(ethDirect)}, nil
}

func (w *Worker) GetBlock(ctx context.Context, block int64) (uint64, error) {
	b, err := w.connect.BlockByNumber(ctx, big.NewInt(block))
	if err != nil {
		return 0, err
	}

	return b.NumberU64(), nil
}
