package geth

import (
	"context"
	"fmt"
	"path"

	"aml/core/database"
)

type Account struct {
	Address     string `csv:"address" db:"address"`
	AccountType int64  `csv:"type" db:"accountType"`
	Date        string `csv:"date" db:"date"`
}

func addTypedAccounts(ctx context.Context, db database.Database, directory string) error {
	var typedAccounts []Account

	err := ParseCSV(path.Join(directory, "blacklist.csv"), &typedAccounts)
	if err != nil {
		return fmt.Errorf("can't parse typedAccounts: %w", err)
	}

	var accounts database.Accounts

	for _, blacklistAccount := range typedAccounts {
		accounts = append(accounts, &database.Account{
			Address: blacklistAccount.Address,
			AccType: database.AccountType(blacklistAccount.AccountType),
		})
	}

	err = accounts.AddAccounts(ctx, db.GetConnection(), false)
	if err != nil {
		return fmt.Errorf("can't add account from typedAccounts: %w", err)
	}

	return AddTokens(ctx, directory, db)
}
