package geth

import (
	"aml/core/database"
	"context"
	"encoding/json"
	"fmt"
	"github.com/ethereum/go-ethereum/common"
	"io/ioutil"
	"os"
	"path"
	"strings"
)

type TokenList map[common.Address]struct{}

func (tl TokenList) IsKnown(address common.Address) bool {
	_, isKnown := tl[address]

	return isKnown
}

type TokenListJSON struct {
	Name   string  `json:"name"`
	Tokens []Token `json:"tokens"`
}

type Token struct {
	ChainID int    `json:"chainId"`
	Address string `json:"address"`
	Name    string `json:"name"`
	Symbol  string `json:"symbol"`
	Decimal int    `json:"decimal"`
	LogoURI string `json:"logoURI"`
}

func AddTokens(ctx context.Context, listsPath string, db database.Database) error {
	files, err := ioutil.ReadDir(listsPath)
	if err != nil {
		return err
	}

	var accounts database.Accounts
	for _, f := range files {
		if f.IsDir() {
			continue
		}

		f, err := os.Open(path.Join(listsPath, f.Name()))
		if err != nil {
			return fmt.Errorf("open file %s with token list error: %w", f.Name(), err)
		}

		fData, err := ioutil.ReadAll(f)
		if err != nil {
			return fmt.Errorf("read token list from file %s error: %w", f.Name(), err)
		}

		tokenList := new(TokenListJSON)

		err = json.Unmarshal(fData, tokenList)
		if err != nil {
			continue
		}

		for _, token := range tokenList.Tokens {
			if token.ChainID != 1 {
				continue
			}

			accounts = append(accounts, &database.Account{
				Address: strings.ToLower(common.HexToAddress(token.Address).String()),
				AccType: database.Token,
			})
		}
	}

	fmt.Println("ADD TOKEN LIST")
	err = accounts.AddAccounts(ctx, db.GetConnection(), false)
	if err != nil {
		return fmt.Errorf("can't add account from typedAccounts: %w", err)
	}

	return nil
}
