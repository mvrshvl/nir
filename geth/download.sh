#!/bin/bash

start_block=$1
end_block=$2
uri=$3
directory=$4

if [[ $uri == *".ipc"* ]]; then
  uri="file://$3"
fi
echo "load blocks"
ethereumetl export_blocks_and_transactions -w 20 --start-block "$start_block" --end-block "$end_block" --blocks-output  "$directory/blocks.csv" --transactions-output  "$directory/transactions.csv" --provider-uri $uri
echo "export tokens"
ethereumetl export_token_transfers -w 20 --start-block "$start_block" --end-block "$end_block" --output  "$directory/token_transfers.csv" --provider-uri $uri
echo "load hashes"
ethereumetl extract_csv_column --input  "$directory/transactions.csv" --column hash --output "$directory/transaction_hashes.txt"
echo "load logs"
ethereumetl export_receipts_and_logs --transaction-hashes  "$directory/transaction_hashes.txt" --logs-output "$directory/logs.csv" --provider-uri $uri || true