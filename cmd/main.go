package main

import (
	"context"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"

	"aml/config"
	"aml/core/clustering"
	"aml/core/database"
	"aml/core/server"
	"aml/core/server/storage"
	"aml/di"
	"aml/geth"
	logging "aml/log"
)

const (
	serverAddress = "localhost:8080"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	ctx, err := prepareDI(ctx)
	if err != nil {
		log.Fatal(err)
	}

	srv := server.New(serverAddress)

	errNotify := make(chan error, 1)

	go func() {
		errNotify <- srv.Run(ctx)
	}()

	err = runBlocksLoader(ctx, errNotify)
	if err != nil {
		log.Fatal(err)
	}

	err = <-errNotify

	logging.Warn(ctx, err)
	fmt.Println(err)
}

func prepareDI(ctx context.Context) (context.Context, error) {
	container, err := di.BuildContainer(
		config.New,
		logging.New,
		database.New,
		geth.New,
		storage.New,
	)
	if err != nil {
		return nil, err
	}

	return di.WithContext(ctx, container), nil
}

func runBlocksLoader(ctx context.Context, errNotify chan error) error {
	return di.FromContext(ctx).Invoke(func(cfg *config.Config) (innerErr error) {
		subscriber, innerErr := geth.LoadBlocks(ctx, cfg.Ethereum.BatchesPoolSize, errNotify)
		if innerErr != nil {
			return innerErr
		}

		go clustering.Run(ctx, subscriber)

		return nil
	})
}
