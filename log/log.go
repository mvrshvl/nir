package log

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"time"

	"github.com/sirupsen/logrus"

	"aml/config"
	"aml/di"
)

func New(cfg *config.Config) (*logrus.Entry, error) {
	logPath := filepath.Join(".", "logs")

	err := os.MkdirAll(logPath, os.ModePerm)
	if err != nil {
		return nil, err
	}

	_, err = os.Create(filepath.Join(logPath, fmt.Sprintf("%s.log", time.Now().String())))
	if err != nil {
		return nil, err
	}

	logger := logrus.New()

	logger.SetOutput(logger.Out)
	logger.SetLevel(cfg.Logger.Level)

	return logrus.NewEntry(logger), nil
}

func Error(ctx context.Context, args ...interface{}) {
	err := di.FromContext(ctx).Invoke(func(l *logrus.Entry) {
		l.Error(args...)
	})
	if err != nil {
		log.Println(err)
	}
}

func Info(ctx context.Context, args ...interface{}) {
	err := di.FromContext(ctx).Invoke(func(l *logrus.Entry) {
		l.Info(args...)
	})
	if err != nil {
		log.Println(err)
	}
}

func Infof(ctx context.Context, format string, args ...interface{}) {
	err := di.FromContext(ctx).Invoke(func(l *logrus.Entry) {
		l.Infof(format, args...)
	})
	if err != nil {
		log.Println(err)
	}
}

func Warn(ctx context.Context, args ...interface{}) {
	err := di.FromContext(ctx).Invoke(func(l *logrus.Entry) {
		l.Warn(args...)
	})
	if err != nil {
		log.Println(err)
	}
}

func Debug(ctx context.Context, args ...interface{}) {
	err := di.FromContext(ctx).Invoke(func(l *logrus.Entry) {
		l.Debug(args...)
	})
	if err != nil {
		log.Println(err)
	}
}

func Debugf(ctx context.Context, format string, args ...interface{}) {
	err := di.FromContext(ctx).Invoke(func(l *logrus.Entry) {
		l.Debugf(format, args...)
	})
	if err != nil {
		log.Println(err)
	}
}

func Errorf(ctx context.Context, format string, args ...interface{}) {
	err := di.FromContext(ctx).Invoke(func(l *logrus.Entry) {
		l.Errorf(format, args...)
	})
	if err != nil {
		log.Println(err)
	}
}

func GetStdout(ctx context.Context) io.Writer {
	var writer io.Writer = os.Stdout

	err := di.FromContext(ctx).Invoke(func(l *logrus.Entry) {
		writer = l.Logger.Out
	})
	if err != nil {
		log.Println(err)
	}

	return writer
}
